#pragma once
#include "Resourses.h"
#include <SFML/Graphics.hpp>
class Game
{
private:
	Txr *blocks_txr;
	Spr *blocks_spr;
	Txr *entity_txr;
	Spr *entity_spr;
public:
	Game();
	Txr& GetBlockTexture();
	Spr& GetBlockSprite();
	Txr& GetEntityTexture();
	Spr& GetEntitySprite();
	~Game();
};