#include "Player.h"
#include "Map.h"
#include "Entity.h"
#include "Game.h"
#include "Resourses.h"
#include <fstream>
#include <string>
#include <SFML/Audio.hpp>
#include <Windows.h>
#define GETMMT map.map[mouse_map_y][mouse_map_x].GetType() // get map mouse type

Player::Player(Map &map, Game &game, int texture_height, int texture_width, float speed_walk, float speed_run)
	:Entity(map,game,texture_height,texture_width,speed_walk,speed_run){
	spr.setTextureRect(sf::IntRect(0, 0, texture_width, texture_height));
}
void Player::Control(Map & map, int game_time, float time){
	static int last_jump = 0;
	if (KPRESS(Kb::D) && AccessToMove(map,Direction::right))x += ((KPRESS(Kb::LShift)) ? speed_run : speed_walk) * time;
	if (KPRESS(Kb::A) && AccessToMove(map, Direction::left))x -= ((KPRESS(Kb::LShift)) ? speed_run : speed_walk) * time;
	if (KPRESS(Kb::Space) && game_time - last_jump >= jump_freq && !is_jump && on_ground) { is_jump = true; last_jump = game_time; }
#ifdef DEBUGCONTROL
	if (KPRESS(Kb::W))y -= 2 * time;
	if (KPRESS(Kb::S))y += 2 * time;
#endif // DEBUG
}
void Player::Camera(RndWnd &wnd, Map & map, Game&game, Vw &camera, float time){
	CameraUpdate(wnd, map, game, camera);
	SetCamera(camera, wnd, 140 ,time);
}
void Player::SetCamera(Vw &view, RndWnd &wnd, int smooth, float time) {
#ifdef DEBUGCAM
	static float debug_move = 180;
	static bool is_set = false;
	if (!is_set) {
		view.setCenter(2000.f, _MAP_HEIGHT / 2 * _TXR_RES + 2000);
		is_set = true;
	}
	if (KPRESS(sf::Keyboard::Right))view.move(debug_move, 0);
	if (KPRESS(sf::Keyboard::Left))view.move(-debug_move, 0);
	if (KPRESS(sf::Keyboard::Up))view.move(0, -debug_move);
	if (KPRESS(sf::Keyboard::Down))view.move(0, debug_move);
	if (KPRESS(sf::Keyboard::Home))view.zoom(0.9f);
	if (KPRESS(sf::Keyboard::End))view.zoom(1.1f);
	if (KPRESS(sf::Keyboard::PageDown))debug_move--;
	if (KPRESS(sf::Keyboard::PageUp))debug_move++;

#else
	static float camera_x = cx, camera_y = cy, E1 = 0.0001f;
	sf::Vector2f camera = view.getCenter();
	static float E2 = 0.1f;
	if (camera_x > cx)camera_x -= (E1 + (camera_x - cx) / smooth) * time;
	if (camera_x < cx)camera_x += (E1 + (cx - camera_x) / smooth) * time;
	if (camera_y > cy)camera_y -= (E1 + (camera_y - cy) / smooth) * time;
	if (camera_y < cy)camera_y += (E1 + (cy - camera_y) / smooth) * time;
	if (KPRESS(sf::Keyboard::V)) {
		camera_x = cx;
		camera_y = cy;
	}
	view.setCenter(camera_x, camera_y);
#endif // DEBUG
	wnd.setView(view);
}
void Player::CameraUpdate(RndWnd & wnd, Map & map, Game & game, Vw & camera){
	static Spr blocks_spr = game.GetBlockSprite();
	//create a chunk texture
	static RndTxr ttxr;
	static Spr tspr;
	static unsigned int render_texture_width  = CUI(_CHUNK_SIZE_X * _TXR_RES);
	static unsigned int render_texture_heigth = CUI(_CHUNK_SIZE_Y * _TXR_RES);
	static int render_texture_width_shift = render_texture_width / _TXR_RES / 2;
	static int render_texture_heigth_shift = render_texture_heigth / _TXR_RES / 2;
	if (ttxr.getSize().x <= 0 || ttxr.getSize().y <= 0)
		ttxr.create(render_texture_width, render_texture_heigth);
	ttxr.clear(sf::Color::White);

	//convertation of the player pixel to the map(array(int))
	static int shift_x = (_WND_WIDTH / _TXR_RES) / 2;
	static int shift_y = (_WND_HEIGHT / _TXR_RES) / 2;
	int px = CI(camera.getCenter().x / _TXR_RES) - render_texture_width_shift;
	int py = CI(camera.getCenter().y / _TXR_RES) - render_texture_heigth_shift;

	for (int i = py, pi = 0; i < _CHUNK_SIZE_Y + py; i++, pi++)
		for (int j = px, pj = 0; j < _CHUNK_SIZE_X + px; j++, pj++)		{
			/*~~~~~~~~~~~~~DRAW ON THE RndTxrt~~~~~~~~~~~~~*/
			if (pi == 0 && pj == 0)	tspr.setPosition(CF(j * _TXR_RES), CF(i* _TXR_RES));
			//output data - for the securty
			int out_i = 0, out_j = 0;
			//i
			if (i >= _MAP_HEIGHT)out_i = _MAP_HEIGHT - 1;
			else if (i < 0)out_i = 0;
			else out_i = i;
			//j
			if (j >= _MAP_WIDTH)out_j = _MAP_WIDTH - 1;
			else if (j < 0)out_j = 0;
			else out_j = j;

			static float os = _TXR_RES / 2;//shifting to the origin(by a texture)
			GetTextureRectByType(blocks_spr, map.map[out_i][out_j].GetType(), map.GetCurrentBiome(j)).
				setPosition(CF(pj * _TXR_RES) + os, CF(pi * _TXR_RES) + os);
			static bool is_origin = false;
			if (!is_origin) {
				blocks_spr.setOrigin(os, os);
				is_origin = true;
			}
			
			//blocks' rotating
			switch (map.map[out_i][out_j].GetRotate())
			{
			case OPR::R0:
				blocks_spr.setRotation(0.f);
				break;
			case OPR::R90:
				blocks_spr.setRotation(90.f);
				break;
			case OPR::R180:
				blocks_spr.setRotation(180.f);
				break;
			case OPR::R270:
				blocks_spr.setRotation(270.f);
				break;
			}
			
			ttxr.draw(blocks_spr);
			//blocks_spr.setOrigin(0, 0);
			/*~~~~~~~~~~~~~OVERLAY~~~~~~~~~~~~~*/
			static float E = 0.001f;//the epsilont
			//damage
			float durablity_block = map.map[out_i][out_j].GetDurablityPercent();
			if (durablity_block < 100.f - E) {
				GetTextureRectDamageBlock(blocks_spr, durablity_block).
					setPosition(CF(pj * _TXR_RES) + os, CF(pi * _TXR_RES) + os);
				ttxr.draw(blocks_spr);
			}

			//grass
			if (out_i - 1 > 0 &&
				map.map[out_i][out_j].GetType() == OPT::grass &&
				map.map[out_i][out_j].GetRotate() == OPR::R0 &&
				map.map[out_i - 1][out_j].GetType() == OPT::air) {
				if (map.GetCurrentBiome(j) == Biomes::summer) {
					blocks_spr.setTextureRect(sf::IntRect(960, 0, _TXR_RES, _TXR_RES));
					blocks_spr.setPosition(CF(pj * _TXR_RES) + os, CF((pi - 1) * _TXR_RES) + os);
				}
				else if (map.GetCurrentBiome(j) == Biomes::winter) {
					blocks_spr.setTextureRect(sf::IntRect(960, _TXR_RES, _TXR_RES, _TXR_RES));
					blocks_spr.setPosition(CF(pj * _TXR_RES) + os, CF((pi - 1) * _TXR_RES) + os);
				}
				ttxr.draw(blocks_spr);
			}
		}

	tspr.setTexture(ttxr.getTexture());
	ttxr.display();
	wnd.draw(tspr);
}
Spr & Player::GetTextureRectDamageBlock(Spr & s, float dp){
	if (dp < 10.f)		s.setTextureRect(sf::IntRect(992, 9 * _TXR_RES, _TXR_RES, _TXR_RES));//992 - is the shifting by X in the texture
	else if (dp < 20.f) s.setTextureRect(sf::IntRect(992, 8 * _TXR_RES, _TXR_RES, _TXR_RES));
	else if (dp < 30.f) s.setTextureRect(sf::IntRect(992, 7 * _TXR_RES, _TXR_RES, _TXR_RES));
	else if (dp < 40.f) s.setTextureRect(sf::IntRect(992, 6 * _TXR_RES, _TXR_RES, _TXR_RES));
	else if (dp < 50.f) s.setTextureRect(sf::IntRect(992, 5 * _TXR_RES, _TXR_RES, _TXR_RES));
	else if (dp < 60.f) s.setTextureRect(sf::IntRect(992, 4 * _TXR_RES, _TXR_RES, _TXR_RES));
	else if (dp < 70.f) s.setTextureRect(sf::IntRect(992, 3 * _TXR_RES, _TXR_RES, _TXR_RES));
	else if (dp < 80.f) s.setTextureRect(sf::IntRect(992, 2 * _TXR_RES, _TXR_RES, _TXR_RES));
	else if (dp < 90.f) s.setTextureRect(sf::IntRect(992, 1 * _TXR_RES, _TXR_RES, _TXR_RES));
	else if (dp < 100.f)s.setTextureRect(sf::IntRect(992, 0, _TXR_RES, _TXR_RES));
	return s;
}
Spr & Player::GetTextureRectByType(Spr & s, OPT type, Biomes biome){
	if (type == OPT::air)				s.setTextureRect(sf::IntRect(_TXR_RES * 5, 0, _TXR_RES, _TXR_RES));
	else if (type == OPT::dirt)			s.setTextureRect(sf::IntRect(_TXR_RES * 2, 0, _TXR_RES, _TXR_RES));
	else if (type == OPT::steel)		s.setTextureRect(sf::IntRect(_TXR_RES * 6, 0, _TXR_RES, _TXR_RES));
	else if (type == OPT::stone)		s.setTextureRect(sf::IntRect(_TXR_RES, 0, _TXR_RES, _TXR_RES));
	else if (type == OPT::cobblestone)	s.setTextureRect(sf::IntRect(_TXR_RES * 3, 0, _TXR_RES, _TXR_RES));
	else if (type == OPT::grass) {
		if (biome == Biomes::summer)	s.setTextureRect(sf::IntRect(_TXR_RES * 4, 0, _TXR_RES, _TXR_RES));
		else if (biome == Biomes::winter)s.setTextureRect(sf::IntRect(_TXR_RES * 4, _TXR_RES, _TXR_RES, _TXR_RES));
		else if (biome == Biomes::desert)s.setTextureRect(sf::IntRect(_TXR_RES * 4, _TXR_RES * 2, _TXR_RES, _TXR_RES));
	}
	else if (type == OPT::deep_stone)	s.setTextureRect(sf::IntRect(_TXR_RES * 7, 0, _TXR_RES, _TXR_RES));
	else if (type == OPT::very_deep_stone)s.setTextureRect(sf::IntRect(_TXR_RES * 8, 0, _TXR_RES, _TXR_RES));
	else if (type == OPT::badrock)		s.setTextureRect(sf::IntRect(_TXR_RES * 9, 0, _TXR_RES, _TXR_RES));
	else if (type == OPT::sandstone)	s.setTextureRect(sf::IntRect(0, _TXR_RES, _TXR_RES, _TXR_RES));
	else if (type == OPT::sand)			s.setTextureRect(sf::IntRect(0, 0, _TXR_RES, _TXR_RES));
	else if (type == OPT::glass)		s.setTextureRect(sf::IntRect(_TXR_RES, _TXR_RES, _TXR_RES, _TXR_RES));
	else if (type == OPT::bulletproof_glass)s.setTextureRect(sf::IntRect(_TXR_RES * 2, _TXR_RES, _TXR_RES, _TXR_RES));

	//wood
	else if (type == OPT::dark_oak)			s.setTextureRect(sf::IntRect(0, _TXR_RES * 3, _TXR_RES, _TXR_RES));
	else if (type == OPT::dark_oak_plank)	s.setTextureRect(sf::IntRect(_TXR_RES, _TXR_RES * 3, _TXR_RES, _TXR_RES));
	else if (type == OPT::dark_oak_leaves)	s.setTextureRect(sf::IntRect(_TXR_RES * 2, _TXR_RES * 3, _TXR_RES, _TXR_RES));

	else if (type == OPT::acacia)			s.setTextureRect(sf::IntRect(_TXR_RES * 3, _TXR_RES * 3, _TXR_RES, _TXR_RES));
	else if (type == OPT::acacia_plank)		s.setTextureRect(sf::IntRect(_TXR_RES * 4, _TXR_RES * 3, _TXR_RES, _TXR_RES));
	else if (type == OPT::acacia_leaves)	s.setTextureRect(sf::IntRect(_TXR_RES * 5, _TXR_RES * 3, _TXR_RES, _TXR_RES));

	else if (type == OPT::spruce)			s.setTextureRect(sf::IntRect(_TXR_RES * 6, _TXR_RES * 3, _TXR_RES, _TXR_RES));
	else if (type == OPT::spruce_plank)		s.setTextureRect(sf::IntRect(_TXR_RES * 7, _TXR_RES * 3, _TXR_RES, _TXR_RES));
	else if (type == OPT::spruce_leaves)	s.setTextureRect(sf::IntRect(_TXR_RES * 8, _TXR_RES * 3, _TXR_RES, _TXR_RES));

	else if (type == OPT::oak)				s.setTextureRect(sf::IntRect(_TXR_RES * 9, _TXR_RES * 3, _TXR_RES, _TXR_RES));
	else if (type == OPT::oak_plank)		s.setTextureRect(sf::IntRect(_TXR_RES * 10, _TXR_RES * 3, _TXR_RES, _TXR_RES));
	else if (type == OPT::oak_leaves)		s.setTextureRect(sf::IntRect(_TXR_RES * 11, _TXR_RES * 3, _TXR_RES, _TXR_RES));

	//ores
	else if (type == OPT::iron)				s.setTextureRect(sf::IntRect(0, _TXR_RES * 2, _TXR_RES, _TXR_RES));
	else if (type == OPT::gold)				s.setTextureRect(sf::IntRect(_TXR_RES, _TXR_RES * 2, _TXR_RES, _TXR_RES));
	else if (type == OPT::niter)			s.setTextureRect(sf::IntRect(_TXR_RES * 2, _TXR_RES * 2, _TXR_RES, _TXR_RES));
	else if (type == OPT::coal)				s.setTextureRect(sf::IntRect(_TXR_RES * 3, _TXR_RES * 2, _TXR_RES, _TXR_RES));
	else if (type == OPT::lead)				s.setTextureRect(sf::IntRect(_TXR_RES * 5, _TXR_RES * 2, _TXR_RES, _TXR_RES));
	else if (type == OPT::diamond)			s.setTextureRect(sf::IntRect(_TXR_RES * 6, _TXR_RES * 2, _TXR_RES, _TXR_RES));
	else if (type == OPT::tin)				s.setTextureRect(sf::IntRect(_TXR_RES * 7, _TXR_RES * 2, _TXR_RES, _TXR_RES));
	else if (type == OPT::copper)			s.setTextureRect(sf::IntRect(_TXR_RES * 8, _TXR_RES * 2, _TXR_RES, _TXR_RES));

	else if (type == OPT::gravel)			s.setTextureRect(sf::IntRect(_TXR_RES * 9, _TXR_RES * 2, _TXR_RES, _TXR_RES));
	else {
		std::string str = std::to_string(CI(type));
		str += " Block not found";
		throw std::exception(str.c_str());
	}
	return s;
}
void Player::MouseAction(RndWnd & wnd, Map & map, Vw &camera, Ms &mouse, int game_time){
	sf::Vector2f mouse_coord = wnd.mapPixelToCoords(mouse.getPosition(wnd));
	int mouse_map_x = CI(mouse_coord.x / _TXR_RES);
	int mouse_map_y = CI(mouse_coord.y / _TXR_RES);
	if (mouse_map_y >= _MAP_HEIGHT)mouse_map_y = _MAP_HEIGHT - 1;
	else if (mouse_map_y < 0)mouse_map_y = 0;
	if (mouse_map_x >= _MAP_WIDTH)mouse_map_x = _MAP_WIDTH - 1;
	else if (mouse_map_x < 0)mouse_map_x = 0;

	static int last_press = 0;
	static int curr_block_i = 0;
#ifdef DEBUGTSM
	static bool mode = 0;//DEBUG 
#endif
	if (KPRESS(Kb::Q) && game_time - last_press >= 200){curr_block_i--; last_press = game_time;}
	if (KPRESS(Kb::E) && game_time - last_press >= 200){curr_block_i++; last_press = game_time;}
	static const int amo_obj = 33;
	if (curr_block_i > amo_obj)curr_block_i = 0;
	if (curr_block_i < 0)curr_block_i = amo_obj;

	OPT curr_block = OPT(curr_block_i);



/*~~~~~~~~~~~~~~~~~~~~~BUTTON PRESSES~~~~~~~~~~~~~~~~~~~~~*/
	//audio
	static bool audio_is_init = false;
	static sf::SoundBuffer sound_buffer;
	static sf::Sound sound;
	if (!audio_is_init) {
		if (!sound_buffer.loadFromFile("audio/click.wav"))
			throw std::exception("Audio 'click.wav' not found");
		audio_is_init = true;
		sound.setBuffer(sound_buffer);
		sound.setPitch(0.75f);
	}


	static int last_press_r = 0;
	static int last_press_left = 0;
	static int last_press_right = 0;
	static int press_delay = 200;//ms
	static float os = _TXR_RES / 2;//shifting to the origin(by a texture)
	static char tmp_rotate{};
#ifdef DEBUGTSM
	if (!mode) {
		if (MPRESS(Mp::Left) && game_time - last_press_left >= press_delay) {
			map.map[mouse_map_y][mouse_map_x].AddDurablity(-55);
			last_press_left = game_time;
		}
		if (MPRESS(Mp::Right) && game_time - last_press_right >= press_delay) {
			map.map[mouse_map_y][mouse_map_x].SetObject(curr_block);
			last_press_right = game_time;
		}
	}
#else
	try	{
		if (MPRESS(Mp::Left) && game_time - last_press_left >= 1 &&
			map.map[mouse_map_y][mouse_map_x].GetType() != OPT::air) {
			map.map[mouse_map_y][mouse_map_x].AddDurablity(-5500);
			last_press_left = game_time;
		}
	}
	catch (const std::exception&ex){
		//check for avialability of the directory
		if (GetFileAttributesA("logs") == 0xFFFFFFFF)	CreateDirectory("logs", NULL);
		//write to file
		std::fstream file("saves/map.dat", std::ios::out | std::ios::binary);
		file.write((char*)ex.what(), strlen(ex.what())* sizeof(*ex.what()));
		file.close();
	}
	if (MPRESS(Mp::Right) && game_time - last_press_right >= press_delay) {
		map.map[mouse_map_y][mouse_map_x].SetObject(curr_block);
		map.map[mouse_map_y][mouse_map_x].SetRotate(OPR(tmp_rotate));
		last_press_right = game_time;
		sound.play();
	}
	if (KPRESS(Kb::R) && game_time - last_press_r >= press_delay) {
		if (tmp_rotate + 1 <= CI(OPR::R270))tmp_rotate++;
		else tmp_rotate = 0;
		last_press_r = game_time;
	}
#endif

	//the block show
	static Img img_mouse_select;
	static Txr txr_mouse_select;
	static Spr spr_mouse_select;
	if (!spr_mouse_select.getTexture()) {
		img_mouse_select.loadFromFile("image/mouse_select.png");
		
		txr_mouse_select.loadFromImage(img_mouse_select);
		spr_mouse_select.setTexture(txr_mouse_select);
	}
	spr_mouse_select.setPosition(CF(mouse_map_x * _TXR_RES), CF(mouse_map_y *_TXR_RES));

	static Txr txr_block_wshow;
	static Spr spr_block_wshow;
	if (!spr_block_wshow.getTexture()) {
		txr_block_wshow.loadFromFile("image/terrain32.png");
		spr_block_wshow.setTexture(txr_block_wshow);
	}
	GetTextureRectByType(spr_block_wshow, curr_block, Biomes::summer)
		.setPosition(mouse_map_x * _TXR_RES + os, mouse_map_y *_TXR_RES + os);
	static bool is_origin_block_show = false;
	if (!is_origin_block_show) {
		spr_block_wshow.setOrigin(os, os);
		is_origin_block_show = true;
	}
	if (tmp_rotate == 0)spr_block_wshow.setRotation(0.f);//R0
	else if (tmp_rotate == 1)spr_block_wshow.setRotation(90.f);//R90
	else if (tmp_rotate == 2)spr_block_wshow.setRotation(180.f);//R180
	else if (tmp_rotate == 3)spr_block_wshow.setRotation(270.f);//R270
	spr_block_wshow.setColor(sf::Color::White);
	wnd.draw(spr_block_wshow);
	wnd.draw(spr_mouse_select);

	//the icon show
	static Txr txr_icon;
	static Spr spr_icon;
	if (!spr_icon.getTexture()) {
		txr_icon.loadFromFile("image/terrain32.png");
		spr_icon.setTexture(txr_icon);
	}
	GetTextureRectByType(spr_icon, curr_block, Biomes::summer).setScale(2.f, 2.f);
	spr_icon.setPosition(camera.getCenter().x - _TXR_RES / 2, camera.getCenter().y + _CAMERA_HEIGHT / 2 - 84);
	wnd.draw(spr_icon);

#ifdef DEBUGTSM
	/*
	TREE-SAVING-MODE(TSM)
	brief: the small addon to saving some tree to the file.
	If you want turn on this OPTion - head to 'Resourses.h' and add or coment 
	out line with define: '#define DEBUGTSM'


	---------------------------------------------------------------------------------
	How it work--->
	The first step: we create some structure;
	The second step: to turn ON press on the button: Z;
	The third step: we stay the pivot(from which will be go count). 
					To stay - press on mouse-button: Right;
	The four step: You can choose the need(that save it to the file) blocks pressing the left mouse-botton;
	The five step: After completion the work - save the work - press on the: Z. 
					Thereby you close the file-stream and saving it;
	---------------------------------------------------------------------------------


	P.s: if you want add the some block to 'SELECT', you can add new 'else if' to 
	the end of branching;
	*/





	static std::fstream out_tree;
	static int last_press_mode = 0;
	static int f_x = 0, f_y = 0;
	//system("cls");
	//std::cout << mode << "\n";
	//std::cout << CI(map.map[mouse_map_y][mouse_map_x].GetType()) << "\n";
	if (KPRESS(Kb::Z) && game_time - last_press_mode >= 500) { 
		if (mode) {
			mode = 0;
			out_tree.close();
			last_press_mode = game_time;
			f_x = 0, f_y = 0;
		} 
		else {
			mode = 1;
			out_tree.open("tree.txt", std::ios::binary | std::ios::out);
			last_press_mode = game_time;
		}
	}
	if (mode) {
		static int last_press_save = 0;
		if (MPRESS(Mp::Right) && f_x == 0 && f_y == 0) { f_y = mouse_map_y; f_x = mouse_map_x; }

		int dif_x{};
		int dif_y{};
		if (MPRESS(Mp::Left) && game_time - last_press_mode >= 200 && f_x != 0 && f_y != 0) {
			dif_x = mouse_map_x - f_x;
			dif_y = mouse_map_y - f_y;
			bool is_write = false;
			//the type-determiner
			if (GETMMT == OPT::dark_oak){
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "dark_oak);\n";
			}
			else if (GETMMT == OPT::dark_oak_leaves) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "dark_oak_leaves);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::dark_oak_plank){
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "dark_oak_plank);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::oak) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "oak);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::oak_leaves) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "oak_leaves);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::oak_plank) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "oak_plank);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::spruce) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "spruce);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::spruce_leaves) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "spruce_leaves);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::spruce_plank) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "spruce_plank);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::acacia) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "acacia);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::acacia_leaves) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "acacia_leaves);\n";
				is_write = true;
			}
			else if (GETMMT == OPT::acacia_plank) {
				out_tree << "m(" << dif_x << "," << dif_y << ").SetObject(OPT::" << "acacia_plank);\n";
				is_write = true;
			}
			if(is_write)map.map[mouse_map_y][mouse_map_x].SetObject(OPT::badrock);
		}
	}
#endif // DEBUGTSM
}
bool Player::NewGame(Map & map){
	Spawn(map);
	this->max_health = 100.f;
	this->health	 = 100.f;
	return false;
}
int Player::GetMapX(){	return mx; }
int Player::GetMapY(){	return my; }