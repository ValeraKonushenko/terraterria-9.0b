#include "Math.h"
#include "Entity.h"
#include "Map.h"
#include "Game.h"
#include "Resourses.h"
#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#define MCX map.map[counter][map_cx].GetType() 
#define	MSLX map.map[counter][map_slx].GetType()
#define	MSRX map.map[counter][map_srx].GetType()

Spr Entity::spr;

Entity::Entity(Map &map, Game &game, int texture_height, int texture_width, 
	float max_health, float health, float mass, 
	float speed_walk,	float speed_run, int jump_freq, 
	int jump_duration, float jump_power):
	texture_width(texture_width),
	texture_height(texture_height),
	speed_walk(speed_walk),
	speed_run(speed_run),
	jump_duration(jump_duration),
	jump_freq(jump_freq),
	jump_power(jump_power),
	mass(mass),
	max_health(max_health),
	health(health)
{
	if(!spr.getTexture())spr = game.GetEntitySprite();
	Spawn(map);
}
void Entity::Spawn(Map & map, int ix, int iy){
	int tx{};
	if (ix < 0 || iy < 0) {
		my = 0;
		tx = _MAP_WIDTH / 2;
		while (map.map[my + 5][tx].GetType() == OPT::air && my + 5 < _MAP_HEIGHT-2)my++;
	}
	else {
		mx = ix;//it needs to edit!!!!!!!
		my = iy;//it needs to edit!!!!!!!
	}
	this->x = CF(tx * _TXR_RES);
	this->y = CF(my * _TXR_RES);
}
void Entity::Update(Map &map, RndWnd &wnd, int game_time){
	bottom_y = y + texture_height;
	cy = y + texture_height / 2;
	right_x = x + texture_width;
	cx = x + texture_width / 2;
	curr_chank = CI((cx / _TXR_RES) / _CHUNK_WIDTH);
	mx = CI(cx / _TXR_RES);
	my = CI(cy / _TXR_RES);

	this->spr.setPosition(x, y);
	wnd.draw(this->spr);

	if (is_jump)JumpUpdate(map, game_time);
	on_ground = Gravity(map, game_time);
	
	float v = CheckForFalling(map);
	if (v > 5)
		health -= v;
#ifdef DEBUG
	static int counter = 10;
	if (counter == 0) {
		system("cls");
		std::cout << "x/y:\t" << x << "/" << y << "\n";
		std::cout << "Chank:\t" << curr_chank << "\n";
		std::cout << "(c)x/y:\t" << cx << "/" << cy << "\n";
		std::cout << "(s)x/y:\t" << right_x << "/" << bottom_y << "\n";
		std::cout << "(mc)x/y\t: " << mx << "/" << my << "\n";
		std::cout << "On ground: " << on_ground << "\n";
		std::cout << "Curr biome: ";
		Biomes tmp = map.GetCurrentBiome(mx);
		if(tmp == Biomes::desert)std::cout << "desert";
		else if(tmp == Biomes::summer)std::cout << "summer";
		else if(tmp == Biomes::winter)std::cout << "winter";
		std::cout << "\n";
		counter = 10;
	}
	else
		counter--;
#endif // DEBUG
}
bool Entity::AccessToMove(Map & map, Direction dir, float distance){
	static OPT bottom;//or left
	static OPT centre;
	static OPT top;//or right
	static const int shifting = this->texture_width / 4;
	if (dir == Direction::down) {
		
	}
	else if (dir == Direction::up) {
		int loc_y = CI((y - distance) / _TXR_RES);
		bottom = map.map[loc_y][CI((cx - shifting) / _TXR_RES)].GetType();
		centre = map.map[loc_y][CI((cx) / _TXR_RES)].GetType();
		top = map.map[loc_y][CI((cx + shifting) / _TXR_RES)].GetType();
	}
	else if (dir == Direction::left) {
		static const int shiftingl = shifting - 4;
		bottom	= map.map[CI((bottom_y - 1) / _TXR_RES)][CI((x + shiftingl - distance) / _TXR_RES)].GetType();
		centre	= map.map[my][CI((x + shiftingl - distance) / _TXR_RES)].GetType();
		top		= map.map[CI(y / _TXR_RES)][CI((x + shiftingl - distance) / _TXR_RES)].GetType();
	}
	else if (dir == Direction::right) {
		static const int shiftingr = shifting - 4;
		bottom	= map.map[CI((bottom_y - 1) / _TXR_RES)][CI((right_x - shiftingr + distance) / _TXR_RES)].GetType();
		centre	= map.map[my][CI((right_x - shiftingr + distance) / _TXR_RES)].GetType();
		top		= map.map[CI(y / _TXR_RES)][CI((right_x - shiftingr + distance) / _TXR_RES)].GetType();
	}

	if (
		BlockIsTransparent(bottom) && 
		BlockIsTransparent(centre) &&
		BlockIsTransparent(top)
		)
		return true;
	else
		return false;
}
void Entity::JumpUpdate(Map & map, int game_time){
	static int start_jump = 0;
	if (start_jump == 0)start_jump = game_time;
	if (AccessToMove(map, Direction::up, 2.f) && game_time - start_jump <= jump_duration)
		y -= jump_power;
	else {
		is_jump = false;
		start_jump = 0;
	}
}
bool Entity::Gravity(Map & map, int game_time){
	static const float gf = _TXR_RES * _HOW_MANY_IS_METR * (GF/1000);
	static int last_time = game_time;
	static float force_accum = 0.f;
	static const float max_gf = _TXR_RES * _HOW_MANY_IS_METR * 50;//max speed falling of the man is 50-60m/s
	static int shifting = this->texture_width / 4;
	int map_y = CI((bottom_y) / _TXR_RES);
	
	//protected from a bug
	if (map_y >= _MAP_HEIGHT)map_y = _MAP_HEIGHT - 1;
	else if (map_y < 0)map_y = 0;

	OPT type_right	= map.map[map_y][CI((cx + shifting) / _TXR_RES)].GetType();
	OPT type_left	= map.map[map_y][CI((cx - shifting) / _TXR_RES)].GetType();
	OPT type_center = map.map[map_y][CI(cx / _TXR_RES)].GetType();
	bool right = BlockIsTransparent(type_right);
	bool left = BlockIsTransparent(type_left);
	bool center = BlockIsTransparent(type_center);

	if ( center && right && left) {
		if (game_time - last_time >= 10) {
			if(force_accum + gf <= max_gf)force_accum += gf;

			//determination of the block on the which an entity is falling
			float bottom_top_block_y{};
			int counter = my;
			int map_cx = mx;//postion on the map(regarding to center an object(by x))
			int map_slx = CI((cx - shifting)/_TXR_RES);//position on the map(slx - ShiftingLeftX)
			int map_srx = CI((cx + shifting)/_TXR_RES);//position on the map(srx - ShiftingRightX)

			//protected from a bug
			if (map_cx >= _MAP_WIDTH)map_cx = _MAP_WIDTH;
			else if (map_cx < 0)map_cx = 0;
			if (map_slx >= _MAP_WIDTH)map_slx = _MAP_WIDTH;
			else if (map_slx < 0)map_slx = 0;
			if (map_srx >= _MAP_WIDTH)map_srx = _MAP_WIDTH;
			else if (map_srx < 0)map_srx = 0;
			if (counter >= _MAP_HEIGHT)	counter = _MAP_HEIGHT-1;
			else if (counter < 0)counter = 0;


			while (counter < _MAP_HEIGHT &&
				BlockIsTransparent(MCX)&&
				BlockIsTransparent(MSLX) &&
				BlockIsTransparent(MSRX)
				)
				counter++;
			bottom_top_block_y = CF(counter * _TXR_RES);//1 - is the epsilont
			float local_bottom_y = y + this->texture_height;
			if (local_bottom_y + force_accum >= bottom_top_block_y) {
				float tmp = bottom_top_block_y - local_bottom_y;
				y += tmp;
			}
			else
				y += force_accum;
			last_time = game_time;
		}
		return false;
	}
	else
		force_accum = 0.f;
	return true;
}
bool Entity::BlockIsTransparent(OPT block)
{
	if(block == OPT::air)					return true;
	else if(block == OPT::acacia)			return true;
	else if(block == OPT::acacia_leaves)	return true;
	else if(block == OPT::oak)				return true;
	else if(block == OPT::oak_leaves)		return true;
	else if(block == OPT::dark_oak)			return true;
	else if(block == OPT::dark_oak_leaves)	return true;
	else if(block == OPT::spruce)			return true;
	else if(block == OPT::spruce_leaves)	return true;
	return false;
}
float Entity::CheckForFalling(Map & map)
{
	static float last_y = y; 
	static int height_passed = 0;//in the pixels
	static float v = 0.f;//the final speed

	//if (y - last_y > 0) {
	//	height_passed += y - last_y;
	//}
	//else {
	//	v = Sqrt(2 * GF * (height_passed / _TXR_RES / 2));
	//	height_passed = 0;
	//}
	//last_y = y;
	//system("cls");
	//std::cout << health;
	return v;
}