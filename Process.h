#pragma once
#include "Math.h"
#include "Object.h"
#include "Map.h"
#include "Entity.h"
#include "Player.h"
#include "Game.h"
#include "Clock.h"
#include "menu_views.h"
#include "Resourses.h"

class Process
{
private:
	bool game_is_play;
	void LoadGame();
protected:
	RndWnd wnd;
	Evnt event;
	Vw camera;
	Game game;
	Map map;
	Player player;
	Clock tm;
	menu_views menu_views;
	Ms mouse;
	int game_time;
	float time;
	inline void EventListener();
	inline void TimeUpdate();
	void GameControl();
public:
	Process();
	void ShowGame();
	void ShowMenu();
	bool isOpen();
	Process(Process&)				= delete;
	Process& operator=(Process&)	= delete;
	~Process()						= default;
};