#include "Math.h"
float Pow(float num, int k) {
	float res{ 1 };
	for (int i = 0; i < k; i++)
		res *= num;
	return res;
}
int Fac(int k) {
	if (k == 0)return 0;
	int res{ 1 };
	for (int i = 2; i <= k; i++)
		res *= i;
	return res;
}
float Sin(float x) {
	float res{};
	for (int k = 0; k < 3; k++)//4 - is the precision
		res += ((k % 2 == 0 ? 1 : -1) * Pow(x, 2 * k + 1)) / Fac(2 * k + 1);
	return res;
}
int Abs(int a) {
	if (a > 0)return a;
	else if (a < 0)return a * -1;
	return a;
}
float fAbs(float a) {
	if (a > 0)return a;
	else if (a < 0)return a * -1;
	return a;
}
float Sqrt(float a) {
	float x{ 1 }, last_x{ 0 };
	float E = 0.01f;
	for (int n = 0; fAbs(last_x - x) > E; n++) {
		last_x = x;
		x = x + 0.5f * (a / x - x);
	}
	return x;
}