#pragma once

float Pow(float num, int k);
int Fac(int k);
float Sin(float x);
int Abs(int a);
float fAbs(float a);
float Sqrt(float a);
const float PI = 3.14159265f;