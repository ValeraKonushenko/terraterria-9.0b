#pragma once
#include <chrono>
typedef std::chrono::time_point <std::chrono::steady_clock> chrono_time;
class Clock
{
	chrono_time last_res;
public:
	int Restart();
	Clock();
	Clock& operator=(Clock&)	= delete;
	Clock(Clock&)				= delete;
	~Clock()					= default;
};

