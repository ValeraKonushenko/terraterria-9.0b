#pragma once
#include "Resourses.h"
#include "Object.h"
#include <random>
#include <SFML/Graphics.hpp>

enum class Biomes {
	none,
	winter,
	summer,
	desert
};
class Map{
private:
	struct Environment {
		void DarkOak0(Map &map, int x, int y);
		void DarkOak1(Map &map, int x, int y);
		void DarkOak2(Map &map, int x, int y);

		void Oak0(Map &map, int x, int y);
		void Oak1(Map &map, int x, int y);
		void Oak2(Map &map, int x, int y);
		void Oak3(Map &map, int x, int y);
		void Oak4(Map &map, int x, int y);

		void Acacia0(Map &map, int x, int y);
		void Acacia1(Map &map, int x, int y);
		void Acacia2(Map &map, int x, int y);
		void Acacia3(Map &map, int x, int y);

		void Spruce0(Map &map, int x, int y);
		void Spruce1(Map &map, int x, int y);
		void Spruce2(Map &map, int x, int y);
		void Spruce3(Map &map, int x, int y);
	}Environment;
	int *biomes_coord;
	Biomes *biomes;
	Object *_map;


	void Generator();
	void BiomeInterpolation(std::default_random_engine &rnd);
	void ClusterGenerator(int x, int y, OPT type, std::default_random_engine &rnd, int amo);
	void DesertClustersGenerator(std::default_random_engine &rnd);
	void PerlinGenerator(std::default_random_engine &rnd);
	float PerlinRand(uint64_t M, uint64_t A, uint64_t C, uint64_t &Z);
	void SurfaceFilling(int x, int from, std::default_random_engine &rnd);
	void TreesPlanter(std::default_random_engine &rnd);
	void Loading(int map_x);
public:
	Biomes GetCurrentBiome(int map_x) const;
	void MapUpdate(int regarding_x, int regarding_y);
	bool NewMap();
	bool LoadMap();
	void SaveMap();
	Object **map;
	Map();
	~Map();
};

