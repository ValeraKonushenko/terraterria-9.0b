#pragma once
#include "Resourses.h"
#include <SFML/Graphics.hpp>

enum class Typemenu_views{
	none,
	NewGame,
	ContinueGame,
	Exit,
	Settings,
	ExitToMenu,
	Resume,
	Title_passive,
	Title_active,
	Inventory,
	Inpect,
	BasicRecipes,
	HoverMaskForButtons
};
enum class Rulemenu_views {
	freq_press_buttons_ms = 200
};
class menu_views {
private:
protected:
	static Txr txr;
	static Spr spr;
	static void Initialize();
	void GetTextureRectByType(Typemenu_views type);
	bool isClick(RndWnd & wnd, Ms &mouse, Typemenu_views type, float x, float y);
	bool isHover(RndWnd & wnd, Ms &mouse, Typemenu_views type, float x, float y);
	sf::Vector2i GetSizemenu_viewsByType(Typemenu_views type);
public:
	static const struct Sizes//pixels in the texture-pack
	{
		static struct NewGame {
			static const uint w = 320;
			static const uint h = 64;
		}NewGame;
		static struct ContinueGame {
			static const uint w = 320;
			static const uint h = 64;
		}ContinueGame;
		static struct Exit {
			static const uint w = 320;
			static const uint h = 64;
		}Exit;
		static struct Settings {
			static const uint w = 320;
			static const uint h = 64;
		}Settings;
		static struct ExitToMenu {
			static const uint w = 320;
			static const uint h = 64;
		}ExitToMenu;
		static struct Resume {
			static const uint w = 320;
			static const uint h = 64;
		}Resume;
		static struct Title_passive {
			static const uint w = 64;
			static const uint h = 64;
		}Title_passive;
		static struct Title_active {
			static const uint w = 64;
			static const uint h = 64;
		}Title_active;
		static struct Inventory {
			static const uint w = 576;
			static const uint h = 24;
		}Inventory;
		static struct Inpect {
			static const uint w = 576;
			static const uint h = 24;
		}Inpect;
		static struct BasicRecipes {
			static const uint w = 320;
			static const uint h = 24;
		}BasicRecipes;
		static struct HoverMaskForButtons {
			static const uint w = 320;
			static const uint h = 64;
		}HoverMaskForButtons;
	}Sizes;
	menu_views();
	bool Button(Ms &mouse, RndWnd &wnd, Typemenu_views type, float x, float y, int game_time);
};