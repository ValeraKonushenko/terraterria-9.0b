#include "Process.h"

inline void Process::EventListener() {
	while (wnd.pollEvent(event))
		if (event.type == Evnt::Closed)
			wnd.close();
}
inline void Process::TimeUpdate(){
	game_time = clock();				//currently time in the game
	time = CF(tm.Restart()) / 1500;		//an interval between the restarts
}
Process::Process():
	wnd(sf::VideoMode(_WND_WIDTH, _WND_HEIGHT), "Game 9.0"),
	player(map, game, 64, 64)
{
	game_is_play = true;
	wnd.setVerticalSyncEnabled(true);
	wnd.setPosition(sf::Vector2i(0, 0));
	camera.reset(sf::FloatRect(0, 0, _CAMERA_WIDTH, _CAMERA_HEIGHT));
	LoadGame();
	game_time = 0;
	time = 0;
}
void Process::LoadGame() {
	Txr txr; 
	if (!txr.loadFromFile("image/load_game.png"))throw std::exception("Texture 'laod_game.png' not found");
	Spr spr(txr);
	EventListener();
	int x = wnd.getSize().x;
	int y = wnd.getSize().y;
	spr.setPosition(CF(x / 2 - 400), CF(y / 2 - 80));//the image res: 800x160
	wnd.clear(sf::Color::White);
	wnd.draw(spr);
	wnd.display();
}
void Process::ShowMenu(){
	bool is_menu = true;
	while (wnd.isOpen() && game_is_play && is_menu) {
		TimeUpdate();
		EventListener();
		wnd.clear(sf::Color::White);

		//view
		float LSW = camera.getCenter().x - _CAMERA_WIDTH/2 + 50.f;//left side of the window
		float USW = camera.getCenter().y - _CAMERA_HEIGHT/2 + 50.f;//up side of the window
		float shifting = 0;
		Typemenu_views out = Typemenu_views::none;

		if (menu_views.Button(mouse, wnd, Typemenu_views::NewGame, LSW, USW + shifting, game_time))
			out = Typemenu_views::NewGame;
		shifting += menu_views::Sizes::NewGame::h + 12;//12 - is just some space

		if(menu_views.Button(mouse, wnd, Typemenu_views::ContinueGame, LSW, USW + shifting, game_time)
			&& out == Typemenu_views::none)
			out = Typemenu_views::ContinueGame;
		shifting += menu_views::Sizes::ContinueGame::h + 12;//12 - is just some space

		if(	menu_views.Button(mouse, wnd, Typemenu_views::Settings, LSW, USW + shifting, game_time)
			&& out == Typemenu_views::none)
			out = Typemenu_views::Settings;
		shifting += menu_views::Sizes::Settings::h + 12;//12 - is just some space

		if (menu_views.Button(mouse, wnd, Typemenu_views::Exit, LSW, USW + shifting, game_time)
			&& out == Typemenu_views::none)
			out = Typemenu_views::Exit;

		if (out == Typemenu_views::Exit)wnd.close();
		else if (out == Typemenu_views::NewGame) {
			map.NewMap();
			player.NewGame(map);
			is_menu = false;
		}

		wnd.display();
	}
}
void Process::ShowGame() {
	while (wnd.isOpen()) {
		TimeUpdate();
		EventListener();

		player.Control(map, game_time, time);
		wnd.clear();
		player.Camera(wnd, map, game, camera, time);
		player.Update(map, wnd, game_time);
		map.MapUpdate(CI(camera.getCenter().x / _TXR_RES), CI(camera.getCenter().y / _TXR_RES));
		player.MouseAction(wnd, map, camera, mouse, game_time);
		GameControl();

		wnd.display();
	}
}
bool Process::isOpen(){
	if(wnd.isOpen())return true;
	return false;
}
void Process::GameControl(){
	static int last_press = game_time;
	if(KPRESS(Kb::Escape) && game_time - last_press >= 300){

	}
}