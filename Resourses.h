#pragma once
#pragma warning(disable: 4293)
//#define DEBUG
//#define DEBUGCAM
//#define DEBUGTSM
//#define DEBUGCONTROL
#include <iostream>

using std::cout;
using std::endl;

typedef unsigned int uint;
typedef unsigned short ushort;
typedef unsigned char uchar;

#define _MAP_HEIGHT		2000
#define _MAP_WIDTH		1280
#define _WND_HEIGHT		720
#define _WND_WIDTH		1280
#define _TXR_RES		32
#define _CAMERA_WIDTH	1920//4000//
#define _CAMERA_HEIGHT	1080//2250//
#define _CHUNK_SIZE_X	(_CAMERA_WIDTH / _TXR_RES + _CAMERA_WIDTH/ _TXR_RES * 0.2)//the size of map's chunk(1024/16 + 1024/16*0.2)
#define _CHUNK_SIZE_Y	(_CAMERA_HEIGHT / _TXR_RES + _CAMERA_HEIGHT/ _TXR_RES * 0.2)//the size of map's chunk(576/16 + 576/16*0.2)
#define _CHUNK_HEIGHT _MAP_HEIGHT
#define _CHUNK_WIDTH 64
#define _CHUNK_AMO	3
#define _HOW_MANY_IS_METR 2

#define GF 9.80665f //gravity force

#define OPT	ObjectProperties::Type
#define OPM ObjectProperties::Mass
#define OPD ObjectProperties::Durablity
#define OPL ObjectProperties::MaxLoad
#define OPR ObjectProperties::Rotate

#define CF(smth) static_cast<float>(smth)
#define CD(smth) static_cast<double>(smth)
#define CI(smth) static_cast<int>(smth)
#define CUI(smth) static_cast<uint>(smth)
#define CUI64(smth) static_cast<uint64_t>(smth)
#define CS(smth) static_cast<short>(smth)
#define CC(smth) static_cast<char>(smth)

#define KPRESS sf::Keyboard::isKeyPressed
#define Kb			sf::Keyboard
#define MPRESS sf::Mouse::isButtonPressed
#define Mp          sf::Mouse
#define Img			sf::Image
#define Txr			sf::Texture
#define Spr			sf::Sprite
#define RndWnd		sf::RenderWindow
#define RndTxr		sf::RenderTexture
#define Evnt		sf::Event
#define Vw			sf::View
#define Ms			sf::Mouse

enum class Direction {
	right,
	left,
	up,
	down
};
enum class Choose {
	newgame,
	loadmap,
};