#pragma once
#include "Object.h"
#include "Resourses.h"
#include <SFML/Graphics.hpp>
class Map;
class Game;
class Entity
{
protected:
	int mx, my				{ 0 };		//currently postion on the map(regarding to cx/cy)
	float x, y				{ 0 };		//currently position on the window
	float right_x, bottom_y	{ 0 };		//right and bottom side the x/y
	float cx, cy			{ 0 };		//center x/y
	bool on_ground, is_jump { 0 };
	static Spr spr;
	int curr_chank;
	int texture_height, texture_width;

	//skills
	float speed_walk, speed_run;
	int jump_duration, jump_freq;
	float jump_power;
	float mass;
	float health;
	float max_health;

	float CheckForFalling(Map &map);
	bool Gravity(Map &map, int game_time);
	void Spawn(Map &map, int x = -1, int y = -1);
	bool AccessToMove(Map &map, Direction dir, float distance = 0.5f);
	void JumpUpdate(Map & map, int game_time);
public:
	Entity(Map &map, Game &game, int texture_height, int texture_width, 
		float max_health = 100.f, float health = 100.f, float mass = 80.f,
		float speed_walk = 0.2f, float speed_run = 0.33f, int jump_freq = 580, 
		int jump_duration = 300, float jump_power = 8);
	virtual void Control(Map &map, int game_time, float time) = 0;
	bool BlockIsTransparent(OPT block);/*return: true - transparent; false - not*/
	void Update(Map &map, RndWnd &wnd, int game_time);
	~Entity() = default;
};