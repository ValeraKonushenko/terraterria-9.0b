#include "Clock.h"
#include <chrono>



Clock::Clock(){
	this->last_res = std::chrono::high_resolution_clock::now();
}

int Clock::Restart()
{
	chrono_time tmp = last_res;
	last_res = std::chrono::high_resolution_clock::now();
	return static_cast<int>(std::chrono::duration_cast<std::chrono::microseconds>(last_res - tmp).count());
}