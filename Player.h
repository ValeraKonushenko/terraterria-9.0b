#pragma once
#include "Resourses.h"
#include "Entity.h"
#include "Object.h"
#include "Map.h"
#include <SFML/Graphics.hpp>

class Map;
class Player : public Entity{
private:
	Spr& GetTextureRectByType(Spr &s, OPT type, Biomes biome);
	void CameraUpdate(RndWnd &wnd, Map &map, Game&game, Vw &camera);
	void SetCamera(Vw &view, RndWnd &wnd, int smooth, float time);
	Spr& GetTextureRectDamageBlock(Spr &s, float durablity_percent);
public:
	Player(Map &map, Game &game, int texture_height, int texture_width, float speed_walk = 0.2f, float speed_run = 0.4f);
	void Control(Map &map, int game_time, float time) override;
	void Camera(RndWnd &wnd, Map &map, Game&game, Vw &camera, float time);
	void MouseAction(RndWnd &wnd, Map &map, Vw &camera, Ms &mouse, int game_time);
	int GetMapX();
	int GetMapY();
	bool NewGame(Map & map);
	bool ContinueGame();
	Player(const Player&)				= delete;
	Player& operator=(const Player&)	= delete;
	~Player()							= default;
};