#include "menu_views.h"
#include "Resourses.h"
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

Txr menu_views::txr;
Spr menu_views::spr;
//sf::RenderWindow menu_views::wnd;

void menu_views::Initialize() {
	if (txr.getSize().x == 0 || txr.getSize().y == 0) {
		if (!txr.loadFromFile("image/gui.png"))
			throw std::exception("Texture 'gui.png' not found");
		else {
			spr.setTexture(txr);
		}
	}
}
menu_views::menu_views() {
	Initialize();
}


bool menu_views::Button(Ms &mouse, RndWnd & wnd, Typemenu_views type, float x, float y, int game_time){
	/////////////////////////
	//audio part
	static sf::SoundBuffer sbuff; 
	if(!sbuff.getSampleRate())sbuff.loadFromFile("audio/click.wav");
	static sf::Sound sound(sbuff);
	/////////////////////////

	static int last_press = game_time;

	GetTextureRectByType(type);
	this->spr.setPosition(x, y);
	wnd.draw(this->spr);
	if (isHover(wnd, mouse, type, x, y)) {
		GetTextureRectByType(Typemenu_views::HoverMaskForButtons);
		this->spr.setPosition(x, y);
		wnd.draw(this->spr);
	}
	if (isClick(wnd, mouse, type, x, y) && game_time - last_press >= CI(Rulemenu_views::freq_press_buttons_ms)) {
		sound.play();
		last_press = game_time;
		return true;
	}

	return false;
}

sf::Vector2i menu_views::GetSizemenu_viewsByType(Typemenu_views type){
	switch (type)
	{
	case Typemenu_views::NewGame:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::ContinueGame:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::Exit:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::Settings:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::ExitToMenu:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::Resume:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::Title_passive:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::Title_active:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::Inventory:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::Inpect:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::BasicRecipes:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	case Typemenu_views::HoverMaskForButtons:
		return sf::Vector2i(menu_views::Sizes::NewGame::w, menu_views::Sizes::NewGame::h);
		break;
	default:
		return sf::Vector2i(-1, -1);
		break;
	}

	return sf::Vector2i();
}

void menu_views::GetTextureRectByType(Typemenu_views type){
	switch (type)
	{
	case Typemenu_views::NewGame:
		this->spr.setTextureRect(sf::IntRect(128, 0, Sizes::NewGame::w, Sizes::NewGame::h));
		break;
	case Typemenu_views::ContinueGame:
		this->spr.setTextureRect(sf::IntRect(448, 0, Sizes::ContinueGame::w, Sizes::ContinueGame::h));
		break;
	case Typemenu_views::Exit:
		this->spr.setTextureRect(sf::IntRect(768, 0, Sizes::Exit::w, Sizes::Exit::h));
		break;
	case Typemenu_views::Settings:
		this->spr.setTextureRect(sf::IntRect(1088, 0, Sizes::Settings::w, Sizes::Settings::h));
		break;
	case Typemenu_views::ExitToMenu:
		this->spr.setTextureRect(sf::IntRect(1408, 0, Sizes::ExitToMenu::w, Sizes::ExitToMenu::h));
		break;
	case Typemenu_views::Resume:
		this->spr.setTextureRect(sf::IntRect(1728, 0, Sizes::Resume::w, Sizes::Resume::h));
		break;
	case Typemenu_views::Title_passive:
		this->spr.setTextureRect(sf::IntRect(0, 0, Sizes::Title_passive::w, Sizes::Title_passive::h));
		break;
	case Typemenu_views::Title_active:
		this->spr.setTextureRect(sf::IntRect(64, 0, Sizes::Title_active::w, Sizes::Title_active::h));
		break;
	case Typemenu_views::Inventory:
		this->spr.setTextureRect(sf::IntRect(0, 64, Sizes::Inventory::w, Sizes::Inventory::h));
		break;
	case Typemenu_views::Inpect:
		this->spr.setTextureRect(sf::IntRect(0, 88, Sizes::Inpect::w, Sizes::Inpect::h));
		break;
	case Typemenu_views::BasicRecipes:
		this->spr.setTextureRect(sf::IntRect(0, 112, Sizes::BasicRecipes::w, Sizes::BasicRecipes::h));
		break;
	case Typemenu_views::HoverMaskForButtons:
		this->spr.setTextureRect(sf::IntRect(576, 64, Sizes::HoverMaskForButtons::w, Sizes::HoverMaskForButtons::h));
		break;
	default:
		break;
	}
}

bool menu_views::isClick(RndWnd & wnd, Ms & mouse, Typemenu_views type, float x, float y){
	sf::Vector2f ms = wnd.mapPixelToCoords(mouse.getPosition(wnd));
	if (ms.x > x && ms.x < x + GetSizemenu_viewsByType(type).x
		&& ms.y > y && ms.y < y + GetSizemenu_viewsByType(type).y
		&& MPRESS(Ms::Left))
		return true;

	return false;
}
bool menu_views::isHover(RndWnd & wnd, Ms & mouse, Typemenu_views type, float x, float y){
	sf::Vector2f ms = wnd.mapPixelToCoords(mouse.getPosition(wnd));
	if(ms.x > x && ms.x < x + GetSizemenu_viewsByType(type).x 
		&& ms.y > y && ms.y < y + GetSizemenu_viewsByType(type).y)
		return true;

	return false;
}