#include "Game.h"
#include "Resourses.h"
#include <SFML/Graphics.hpp>
#include <fstream>


Game::Game(){
	const std::string DIR_TO_IMG_BLOCKS = "image/terrain32.png";
	const std::string DIR_TO_IMG_ENTITY = "image/entity.png";
	const std::string DIR_TO_REPORTS = "report" + std::to_string(time(0)) + ".txt";

	//block texture
	blocks_txr = new Txr;
	blocks_spr = new Spr;
	if (!blocks_txr->loadFromFile(DIR_TO_IMG_BLOCKS)) {
		std::fstream report(DIR_TO_REPORTS, std::ios::out | std::ios::binary);
		report << "Failed to open the texture of the blocks";
		report.close();
	}
	blocks_spr->setTexture(*blocks_txr);

	//entity texture
	entity_txr = new Txr;
	entity_spr = new Spr;
	if (!entity_txr->loadFromFile(DIR_TO_IMG_ENTITY)) {
		std::fstream report(DIR_TO_REPORTS, std::ios::out | std::ios::binary);
		report << "Failed to open the texture of the blocks";
		report.close();
	}
	entity_spr->setTexture(*entity_txr);
}


Game::~Game(){
	delete 
		entity_spr, entity_txr,
		blocks_txr, blocks_spr;
}

Spr & Game::GetBlockSprite()	{ return *blocks_spr;}
Txr & Game::GetBlockTexture()	{ return *blocks_txr;}
Spr & Game::GetEntitySprite()	{ return *entity_spr; }
Txr & Game::GetEntityTexture()	{ return *entity_txr; }