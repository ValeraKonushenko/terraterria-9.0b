#pragma once
#include "Resourses.h"
#include <exception>
#pragma pack(push,1)
namespace ObjectProperties {
	enum class Mass
	{
		air = 0,
		steel = 96,
		stone = 18,
		dirt = 14,
		grass = 13,
		cobblestone = 17,
		deep_stone = 24,
		very_deep_stone = 32,
		badrock = 2147483647,
		sandstone = 18,
		sand = 12,
		glass = 6,
		bulletproof_glass = 24,
		gravel = 14,

		//wood
		dark_oak = 8,
		dark_oak_plank = 8,
		dark_oak_leaves = 1,

		spruce = 9,
		spruce_plank = 9,
		spruce_leaves = 1,

		oak = 8,
		oak_plank = 8,
		oak_leaves = 1,

		acacia = 9,
		acacia_plank = 9,
		acacia_leaves = 1,

		//ores
		iron = 22,
		gold = 20,
		niter = 19,
		coal = 19,
		lead = 20,
		diamond = 26,
		tin = 21,
		copper = 21
	};
	enum class Type:ushort
	{
		air,
		steel,
		stone,
		dirt,
		grass,
		cobblestone,
		deep_stone,
		very_deep_stone,
		badrock,
		sandstone,
		sand,
		glass,
		bulletproof_glass,
		gravel,

		//wood
		dark_oak,
		dark_oak_plank,
		dark_oak_leaves,

		spruce,
		spruce_plank,
		spruce_leaves,

		oak,
		oak_plank,
		oak_leaves,

		acacia,
		acacia_plank,
		acacia_leaves,

		//ores
		iron,
		gold,
		niter,
		coal,
		lead,
		diamond,
		tin,
		copper
	};
	enum class MaxLoad
	{
		air = 0,
		steel = 900,
		stone = 60,
		dirt = 40,
		grass = 40,
		cobblestone = 50,
		deep_stone = 80,
		very_deep_stone = 110,
		badrock = 2147483647,
		sandstone = 52,
		sand = 30,
		glass = 11,
		bulletproof_glass = 71,
		gravel = 42,

		//wood
		dark_oak = 72,
		dark_oak_plank = 60,
		dark_oak_leaves = 9,

		spruce = 48,
		spruce_plank = 40,
		spruce_leaves = 9,

		oak = 72,
		oak_plank = 55,
		oak_leaves = 9,

		acacia = 54,
		acacia_plank = 44,
		acacia_leaves = 9,

		//ores
		iron = 70,
		gold = 62,
		niter = 61,
		coal = 61,
		lead = 65,
		diamond = 90,
		tin = 66,
		copper = 66
	};
	enum class Durablity
	{
		air = 1,
		steel = 3200,
		stone = 600,
		dirt = 200,
		grass = 150,
		cobblestone = 500,
		deep_stone = 900,
		very_deep_stone = 1200,
		badrock = 2147483647,
		sandstone = 400,
		sand = 120,
		glass = 40,
		bulletproof_glass = 1000,
		gravel = 200,

		//wood
		dark_oak		= 270,
		dark_oak_plank	= 210,
		dark_oak_leaves	= 20,

		spruce			= 220,
		spruce_plank	= 170,
		spruce_leaves	= 20,

		oak				= 260,
		oak_plank		= 200,
		oak_leaves		= 20,

		acacia			= 330,
		acacia_plank	= 260,
		acacia_leaves	= 20,

		//ores
		iron = 1000,
		gold = 500,
		niter = 450,
		coal = 450,
		lead = 600,
		diamond = 1600,
		tin = 500,
		copper = 500
	};
	enum class Rotate:uchar {
		R0,
		R90,
		R180,
		R270,
	};
}
class Map;
class Object
{
private:
	OPT type;
	float durablity;
	OPR rotate;
	//float mass			{ 0 };//it needs to delete it
	//float accum_load	{ 0 };
	//bool on_ground		{ 0 };
public:
	Object();
	Object(OPT type);
	float GetDurablity()const;
	float GetDurablityPercent()const;
	OPR GetRotate()const;
	void SetRotate(OPR rotation);
	//float GetAccumulateLoad();
	//bool AddAccumulateLoad(float add);
	bool AddDurablity(float add);
	void SetObject(OPT type);
	float GetMaxLoad(OPT type)const;
	float GetConstDurablity(OPT type)const;
	float GetConstMass(OPT type)const;
	OPT GetType()const;
	void Update();
	Object(const Object &)				= delete;
	Object(const Object &&)				= delete;
	Object& operator=(const Object &)	= delete;
	~Object() = default;
};

#pragma pack(pop)