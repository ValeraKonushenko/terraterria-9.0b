#include "Resourses.h"
#include "Map.h"
#include "Math.h"
#include <ctime>
#include <cmath>
#include <random>
#include <iostream>
#include <fstream>
#include <SFML/Graphics.hpp>
#include <Windows.h>
enum Generator{
	//Biome Interpolation
	biome_interpolation_min = 1,//width of the smooth
	biome_interpolation_max = 7,//width of the smooth


	//desert clusters
	desert_cluster_rnd = 200,
	desert_cluster_width = 20,
	desert_cluster_deep_min = 2,
	desert_cluster_deep_max = 5,//plus min

	//trees
	density_trees_rnd = 40,
	density_trees = 10,//how far the trees will be stay to each other

	//!!GENERAL!! map generator
	amplitude = 50,
	wave = 50,
	min_deep_very_deep_stone = 400,
	max_deep_very_deep_stone = 7,//+ min_deep
	min_deep_deep_stone = 600,//+ min_deep_very_deep_stone + max_deep_very_deep_stone
	max_deep_deep_stone = 7,//+ min_deep

	//dirt gen
	dirt_min = 10,
	dirt_max = 10,//plus dirt_min
	sandstone_min = 7,
	sandstone_max = 3,

	amplitude_rnd = 20,//freq
	amplitude_lvl1 = 20,
	amplitude_lvl2 = 30,
	amplitude_lvl3 = 50,
	amplitude_lvl4 = 70,
	amplitude_lvl5 = 90,
	amplitude_lvl6 = 120,
	amplitude_step = 30,

	wave_rnd = 20,//freq
	wave_lvl1 = 10,
	wave_lvl2 = 20,
	wave_lvl3 = 30,
	wave_lvl4 = 100,

	height_rnd = 2,

	//ores
	ores_rnd = 2000,
	chance_of_ore = 3,//if more - less the ores and more the gravel
	max_cluster = 300,
	min_cluster = 5
};
Map::Map() {
	try {
		////////////////////////////////////////////////////////////////
		//////////////////////IT NEEDS an EDIT//////////////////////////
		biomes_coord = new int[4]{ 0, _MAP_WIDTH / 3, _MAP_WIDTH / 3 * 2, _MAP_WIDTH };//for three biomes!!!!!!
		biomes = new Biomes[3]{ Biomes::desert,Biomes::summer,Biomes::winter };
		////////////////////////////////////////////////////////////////

		_map = new Object[_MAP_HEIGHT * _MAP_WIDTH];
		map = new Object*[_MAP_HEIGHT];
		for (int i = 0; i < _MAP_HEIGHT; i++)
			map[i] = _map + i * _MAP_WIDTH;
	}
	catch (const std::bad_alloc) {
		//check for avialability of the directory
		if (GetFileAttributesA("logs") == 0xFFFFFFFF)
			CreateDirectory("logs", NULL);
		//write to file
		std::fstream file("saves/map.dat", std::ios::out | std::ios::binary);
		file.write((char*)"Possible, you have a small RAM", 31);
		file.close();
	}	
	////debug
	//while (1) {
	//	static int last_enter = clock();
	//	if (clock() - last_enter >= 30) {
	//		static int spx = 0, spy = 0;
	//		system("cls");
	//		std::cout << spx << " " << spy << std::endl;
	//		if (KPRESS(sf::Keyboard::D) && spx + 1 < _MAP_WIDTH)spx++;
	//		if (KPRESS(sf::Keyboard::A) && spx - 1 >= 0)spx--;
	//		if (KPRESS(sf::Keyboard::W) && spy - 1 >= 0)spy--;
	//		if (KPRESS(sf::Keyboard::S) && spy + 1 < _MAP_HEIGHT)spy++;
	//		OPT temp = map[spy][spx].GetType();
	//		if (temp == OPT::air)std::cout << "Air";
	//		else if (temp == OPT::wood)std::cout << "Wood";
	//		else if (temp == OPT::dirt)std::cout << "Dirt";
	//		else if (temp == OPT::steel)std::cout << "Steel";
	//		else if (temp == OPT::stone)std::cout << "Stone";
	//		std::cout << "\n";
	//		std::cout << "Durablity: " << map[spy][spx].GetDurablity() << "/" << map[spy][spx].GetConstDurablity(map[spy][spx].GetType()) << std::endl;
	//		std::cout << "Load: " << map[spy][spx].GetAccumulateLoad() << "/" << map[spy][spx].GetMaxLoad(map[spy][spx].GetType()) << std::endl;
	//		if (KPRESS(sf::Keyboard::Space))map[spy][spx].AddDurablity(-5);
	//		if (KPRESS(sf::Keyboard::LShift))map[spy][spx].AddDurablity(5);
	//		if (KPRESS(sf::Keyboard::T))map[spy][spx].SetObject(OPT::dirt);
	//		if (KPRESS(sf::Keyboard::G))map[spy][spx].SetObject(OPT::wood);
	//		if (KPRESS(sf::Keyboard::B))map[spy][spx].SetObject(OPT::steel);
	//		if (KPRESS(sf::Keyboard::Z))break;
	//		map[spy][spx].Update(*this);
	//		last_enter = clock();
	//	}
	//}
	//
	//for (int i = 0; i < _MAP_HEIGHT; i++)
	//{
	//	for (int j = 0; j < _MAP_WIDTH; j++)
	//	{
	//		OPT temp = map[i][j].GetType();
	//		if (temp == OPT::air)std::cout << " ";
	//		else if (temp == OPT::wood)std::cout << "1";
	//		else if (temp == OPT::dirt)std::cout << "2";
	//		else if (temp == OPT::steel)std::cout << "3";
	//		else if (temp == OPT::stone)std::cout << "4";
	//	}
	//	std::cout << "\n";
	//}
}
Biomes Map::GetCurrentBiome(int map_x) const{
	int i = 0;
	for (; i < 3; i++)//3 biomes
		if (map_x >= biomes_coord[i] && map_x <= biomes_coord[i + 1])
			break;
	return biomes[i];
}
void Map::SurfaceFilling(int x, int from, std::default_random_engine &rnd){
/*
~~~~~~~~~~~~~SurfaceFilling~~~~~~~~~~~~~

In this function we fill in the surface of the map, more precisely surface 
- that part of the map, which below of the grass/dirt/sand.
I devide it by the levels:

	1 level - surface of the grass/sand
	2 level - surface under the '1 level' - dirt/sand(if it's a desert+sandstone)
	3 level - surface of the stone
	n level - the lower block on the map - badrock
	n-1 level - we push off from the badrock on the level upper - there a 
				'very deep stone'. Approximate height:
				from _MAP_HEIGH - 1 to ~_MAP_HEIGH - 100
	n-2 level - there is 'deep stone'. Approximate height of this part:
				from ~_MAP_HEIGH - 100 to ~_MAP_HEIGH - 500

Also:
	In some part by the height 
	I create ore-branches(cluster), or just a cluster of somethings.
	For example: in the interval between some levels I spawn more or less of
	ores. Also, depends on height amount and type of it.
*/
	//dirt - level 1
	int dirt_rnd = rnd() % dirt_max + dirt_min;
	int y_dirt_lim = dirt_rnd + from;

	//level 2
	if (GetCurrentBiome(x) == Biomes::desert) {
		for (; from < y_dirt_lim && from < _MAP_HEIGHT; from++)
			map[from][x].SetObject(OPT::sand);
		int sandstone_rnd = rnd() % sandstone_max + sandstone_min + from;
		for (; from < sandstone_rnd && from < _MAP_HEIGHT; from++)
			map[from][x].SetObject(OPT::sandstone);
	}
	else
		for (; from < y_dirt_lim && from < _MAP_HEIGHT; from++)
			map[from][x].SetObject(OPT::dirt);

	//level 3
	for (; from < _MAP_HEIGHT; from++) {
		map[from][x].SetObject(OPT::stone);
		if (rnd() % ores_rnd == 0 && map[from - 20][x].GetType() != OPT::air) {
			int what_ore = rnd() % 101;//as 100%
			if (what_ore >= 0 && 26 >= what_ore)
				ClusterGenerator(x, from, OPT::iron, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 27 && 44 >= what_ore)
				ClusterGenerator(x, from, OPT::lead, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 45 && 54 >= what_ore)
				ClusterGenerator(x, from, OPT::copper, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 55 && 61 >= what_ore)
				ClusterGenerator(x, from, OPT::tin, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 62 && 67 >= what_ore)
				ClusterGenerator(x, from, OPT::coal, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 68 && 68 >= what_ore)
				ClusterGenerator(x, from, OPT::gold, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 69 && 71 >= what_ore)
				ClusterGenerator(x, from, OPT::niter, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 72 && 82 >= what_ore)
				ClusterGenerator(x, from, OPT::sand, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 83 && 89 >= what_ore)
				ClusterGenerator(x, from, OPT::sand, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 90 && 100 >= what_ore)
				ClusterGenerator(x, from, OPT::sand, rnd, CI(rnd() % max_cluster + min_cluster));
		}
	}

	
	//level n
	int bottom = _MAP_HEIGHT - 1;
		map[bottom--][x].SetObject(OPT::badrock);

	//level n - 1
	int top_very_deep_stone = _MAP_HEIGHT - (min_deep_very_deep_stone + rnd() % max_deep_very_deep_stone);
	while (bottom > top_very_deep_stone && bottom >= 0) {
		map[bottom][x].SetObject(OPT::very_deep_stone);
		if (rnd() % ores_rnd == 0) {
			int what_ore = rnd() % 101;//as 100%
			if (what_ore >= 0 && 2 >= what_ore)
				ClusterGenerator(x, bottom, OPT::iron, rnd, CI(rnd() % max_cluster + min_cluster));
			else if(what_ore >= 3 && 3 >= what_ore)
				ClusterGenerator(x, bottom, OPT::lead, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 4 && 4 >= what_ore)
				ClusterGenerator(x, bottom, OPT::tin, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 5 && 50 >= what_ore)
				ClusterGenerator(x, bottom, OPT::coal, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 51 && 62 >= what_ore)
				ClusterGenerator(x, bottom, OPT::diamond, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 63 && 80 >= what_ore)
				ClusterGenerator(x, bottom, OPT::gold, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 80 && 100 >= what_ore)
				ClusterGenerator(x, bottom, OPT::niter, rnd, CI(rnd() % max_cluster + min_cluster));

		}
		bottom--;
	}

	//level n - 2
	int top_deep_stone = _MAP_HEIGHT - (min_deep_deep_stone + rnd() % max_deep_deep_stone);
	while (bottom > top_deep_stone && bottom >= 0 && map[bottom - 20][x].GetType() != OPT::air){
		map[bottom][x].SetObject(OPT::deep_stone);
		if (rnd() % ores_rnd == 0 && map[bottom - 20][x].GetType() != OPT::air) {
			int what_ore = rnd() % 101;//as 100%
			if (what_ore >= 0 && 7 >= what_ore)
				ClusterGenerator(x, bottom, OPT::iron, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 8 && 12 >= what_ore)
				ClusterGenerator(x, bottom, OPT::lead, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 13 && 14 >= what_ore)
				ClusterGenerator(x, bottom, OPT::copper, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 14 && 17 >= what_ore)
				ClusterGenerator(x, bottom, OPT::tin, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 18 && 66 >= what_ore)
				ClusterGenerator(x, bottom, OPT::coal, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 67 && 69 >= what_ore)
				ClusterGenerator(x, bottom, OPT::diamond, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 70 && 81 >= what_ore)
				ClusterGenerator(x, bottom, OPT::gold, rnd, CI(rnd() % max_cluster + min_cluster));
			else if (what_ore >= 82 && 100 >= what_ore)
				ClusterGenerator(x, bottom, OPT::niter, rnd, CI(rnd() % max_cluster + min_cluster));

		}
		bottom--;
	}
}
/*void Map::PerlinGenerator(std::default_random_engine &rnd){
	uint	M = CUI((1 << 32) - 1);
	uint	A = CUI(1'664'525),
			C = CUI(1);
	uint	Z = CUI(static_cast<uint>((CF(rnd() % 1'000'000'000) / 1'000'000'000) * M));

	int	x = 0;
	int	y = _MAP_HEIGHT / 2;
	int amp = amplitude;
	int wv = wave;
	float a = PerlinRand(M, A, C, Z);
	float b = PerlinRand(M, A, C, Z);

	while (x < _MAP_WIDTH) {
		static int start_x = 0;
		static int start_y = 0;
		if (x % wv == 0) {
			a = b;
			b = PerlinRand(M, A, C, Z);
			y = CI(_MAP_HEIGHT - (_MAP_HEIGHT / 3) + a * amp);
			int y_rnd = rnd() % extreme_gen_rnd;
			if (y_rnd == 0)y += 10 + rnd() % 20;
			else if (y_rnd == 1)y -= 10 + rnd() % 20;

			//interpolation
			if (x != 0) {		
				int last_y = y;
				int last_x = x;
				for (int i = x; i > x - wv; i--)
				{
					int ty = CI(round(CF(-(start_y - last_y) * i) / (last_x - start_x) + CF(-(start_x * last_y - last_x * start_y)) / (last_x - start_x)));
					if (GetCurrentBiome(i) == Biomes::desert)
						map[ty][i].SetObject(OPT::sand);
					else
						map[ty][i].SetObject(OPT::grass);
					SurfaceFilling(i, ty + 1, rnd);
					//Draw(i, ty, '0');
				}
			}
			
			//amplitude - rnd
			static int last_amp = amp;
			static int amo_step_amp = 0;
			if (rnd() % amplitude_rnd == 0) {
				last_amp = amp;
				int l_tmp = rnd() % 6;
				if (l_tmp == 0)amp = amplitude_lvl1;
				else if (l_tmp == 0)amp = amplitude_lvl2;
				else if (l_tmp == 0)amp = amplitude_lvl3;
				else if (l_tmp == 0)amp = amplitude_lvl4;
				else if (l_tmp == 0)amp = amplitude_lvl5;
				else if (l_tmp == 0)amp = amplitude_lvl6;
				amo_step_amp = rnd() % amplitude_step;
			}
			if (amo_step_amp != 0)amo_step_amp--;
			else	amp = last_amp;

			//wave lenght - rnd
			static int last_wl = amp;
			static int amo_step_wv = 0;
			if (rnd() % wave_rnd == 0) {
				last_wl = amp;
				int l_tmp = rnd() % 4;
				if (l_tmp == 0)wv = wave_lvl1;
				else if (l_tmp == 0)wv = wave_lvl2;
				else if (l_tmp == 0)wv = wave_lvl3;
				else if (l_tmp == 0)wv = wave_lvl4;
				amo_step_wv = rnd() % 40;
			}
			if (amo_step_wv != 0)amo_step_wv--;
			else	wv = last_wl;
			
			start_x = x;
			start_y = y;

			if (GetCurrentBiome(x) == Biomes::desert)
				map[y][x].SetObject(OPT::sand);
			else
				map[y][x].SetObject(OPT::grass);
		}
		x += 1;
	}
}*/

void Map::PerlinGenerator(std::default_random_engine &rnd) {
	uint64_t	M = CUI64(~0u);
	uint64_t	A = CUI64(1664525);
	uint64_t	C = CUI64(1013904223);
	uint64_t	Z = CUI64(static_cast<uint>((CF(rnd() % 1'000'000'000) / 1'000'000'000) * M));

	int	x = 0;
	int	y = _MAP_HEIGHT / 3;
	int amp = amplitude;
	int wv = wave;
	float a = PerlinRand(M, A, C, Z);
	float b = PerlinRand(M, A, C, Z);

	while (x < _MAP_WIDTH) {
		static int x0 = 0;
		static int y0 = 0;
		if (x % wv == 0) {
			a = b;
			b = PerlinRand(M, A, C, Z);
			static float devider = 2.f;
			y = CI(_MAP_HEIGHT - (_MAP_HEIGHT / devider) + a * amp);

			//interpolation
			if (x != 0) {
				int y1 = y;
				int x1 = x;
				float step = 1.f / wv;
				float t = 0.f;
				for (int i = x0; i < x1; t+= step, i++){
					//int ty = CI(round(CF(-(y0 - y1) * i) / (x1 - x0) + CF(-(x0 * y1 - x1 * y0)) / (x1 - x0)));
					float ttt = -cos(t * PI) / 2 + 0.5f;
					int ty = CI(y0 + ttt * (y1-y0));
					if (ty - 100 < 0)ty = 100;
					else if (ty + 100 > _MAP_HEIGHT - 1)ty = _MAP_HEIGHT - 100;
					if (GetCurrentBiome(x0) == Biomes::desert)
						map[ty][i].SetObject(OPT::sand);
					else
						map[ty][i].SetObject(OPT::grass);
					SurfaceFilling(i, ty + 1, rnd);
					//Draw(i, ty, '0');
				}
			}

			//amplitude - rnd
			static int last_amp = amp;
			static int amo_step_amp = 0;
			if (rnd() % amplitude_rnd == 0 && amo_step_amp == 0) {
				last_amp = amp;
				int l_tmp = rnd() % 6;
				if (l_tmp == 0)amp = amplitude_lvl1;
				else if (l_tmp == 1)amp = amplitude_lvl2;
				else if (l_tmp == 2)amp = amplitude_lvl3;
				else if (l_tmp == 3)amp = amplitude_lvl4;
				else if (l_tmp == 4)amp = amplitude_lvl5;
				else if (l_tmp == 5)amp = amplitude_lvl6;
				amo_step_amp = rnd() % amplitude_step;
			}
			if (amo_step_amp != 0)amo_step_amp--;
			else	amp = last_amp;

			////wave lenght - rnd
			//static int last_wl = amp;
			//static int amo_step_wv = 0;
			//if (rnd() % wave_rnd == 0 && amo_step_wv == 0) {
			//	last_wl = amp;
			//	int l_tmp = rnd() % 4;
			//	if (l_tmp == 0)wv = wave_lvl1;
			//	else if (l_tmp == 1)wv = wave_lvl2;
			//	else if (l_tmp == 2)wv = wave_lvl3;
			//	else if (l_tmp == 3)wv = wave_lvl4;
			//	amo_step_wv = rnd() % 40;
			//}
			//if (amo_step_wv != 0)amo_step_wv--;
			//else	wv = last_wl;

			if (rnd() % height_rnd == 0) {
				static int height_epsilont = 20;
				int trnd = rnd() % 2;
				if (trnd == 0 && devider - 0.01f > 1.15f)devider -= 0.01f;
				else if (trnd == 0 && devider + 0.01f < 8)devider += 0.01f;
			}

			x0 = x;
			y0 = y;

			if (GetCurrentBiome(x) == Biomes::desert)
				map[y][x].SetObject(OPT::sand);
			else
				map[y][x].SetObject(OPT::grass);
		}
		x += 1;
		//A+=5;
		//C++;
		//cout << y << endl;
	}
}
void Map::TreesPlanter(std::default_random_engine & rnd){
	for (int i = 0; i < _MAP_WIDTH; i++){
		int trnd{};
		if(rnd()% 2 == 0)
			trnd = rnd() %  (density_trees_rnd*4);
		else
			trnd = rnd() %  density_trees_rnd;

		Biomes c_biome = GetCurrentBiome(i);//current biome
		int j{0};
		while (j + 1 < _MAP_HEIGHT && map[j+1][i].GetType() == OPT::air)j++;
		static int last_tree_x = 0;
		//static bool debug_bool = false;
		//if (!debug_bool && i == 128) {
		//	Environment.Acacia2(*this, i, j); 
		//	debug_bool = true;
		//}
		if (j < _MAP_HEIGHT-2 && trnd == 0 && i - last_tree_x >= density_trees && 
			(map[j + 1][i].GetType() == OPT::grass || map[j + 1][i].GetType() == OPT::dirt)) 
		{
			if (c_biome == Biomes::summer) {
				int srnd = rnd() % 20;
				if(srnd == 0)		Environment.DarkOak0(*this, i, j);
				else if(srnd == 1)	Environment.DarkOak1(*this, i, j);
				else if(srnd == 2)	Environment.DarkOak2(*this, i, j);
				else if(srnd == 3)	Environment.Oak0(*this, i, j);
				else if(srnd == 4)	Environment.Oak1(*this, i, j);
				else if(srnd == 5)	Environment.Oak3(*this, i, j);
				else if(srnd == 6)	Environment.Oak4(*this, i, j);
				else 	Environment.Oak2(*this, i, j);
			}
			else if (c_biome == Biomes::desert) {
				int drnd = rnd() % 10;
				if (drnd == 0)		Environment.Acacia0(*this, i, j);
				else if (drnd == 1) Environment.Acacia1(*this, i, j);
				else if (drnd == 2) Environment.Acacia2(*this, i, j);
				else				Environment.Acacia3(*this, i, j);
			}
			else if (c_biome == Biomes::winter) {
				int wrnd = rnd() % 10;
				if (wrnd == 0)			Environment.Spruce0(*this, i, j);
				else if (wrnd == 1)		Environment.Spruce1(*this, i, j);
				else if (wrnd == 2)		Environment.Spruce2(*this, i, j);
				else					Environment.Spruce3(*this, i, j);
			}
			last_tree_x = i;
		}
	}

}
float Map::PerlinRand(uint64_t M, uint64_t A, uint64_t C, uint64_t & Z){
	Z = (A * Z + C) % M;
	return CF(Z) / M;
}
void Map::Generator(){
	std::default_random_engine rnd(5);
	std::fstream file("saves/map.dat", std::ios::out | std::ios::binary);

	//clearing
	for (int i = 0; i < _MAP_HEIGHT * _MAP_WIDTH; i++)
		_map[i].SetObject(OPT::air);

	//map-generator
	for (int i = 0; i < _MAP_WIDTH; i++)
	{
		static int piv = _CHUNK_HEIGHT / 2;
		int t = rnd() % 15;
		if (t == 1 && piv + 1 < _CHUNK_HEIGHT)piv++;
		if (t == 0 && piv - 1 > 0)piv--;

		map[piv][i].SetObject(OPT::dirt);

		for (int j = piv + 1; j < _CHUNK_HEIGHT; j++)
			map[j][i].SetObject(OPT::stone);

	}

	file.write((char*)_map, _MAP_HEIGHT * _MAP_WIDTH * sizeof(*_map));
	file.close();
}
void Map::Loading(int chunk){
	//declaration different variables
	int beg_chunk = (chunk - _CHUNK_AMO / 2 < 0)?0: chunk - _CHUNK_AMO / 2;
	//int end_chunk = (beg_chunk + _CHUNK_AMO > _MAP_WIDTH/_CHUNK_WIDTH)? _MAP_WIDTH / _CHUNK_WIDTH : beg_chunk + _CHUNK_AMO;
	static int sizeof_Object = sizeof(Object);
	static Object *_buff = new Object[_CHUNK_HEIGHT * _CHUNK_WIDTH * _CHUNK_AMO];
	static Object **buff = new Object*[_CHUNK_HEIGHT];
	static int download_volume = _CHUNK_HEIGHT * _CHUNK_WIDTH * _CHUNK_AMO * sizeof_Object;//in byte
	static int volume_chank = _CHUNK_HEIGHT * _CHUNK_WIDTH;
	static bool is_enter = false;
	if(!is_enter)
		for (int i = 0; i < _CHUNK_HEIGHT; i++)
			buff[i] = _buff + i * _CHUNK_WIDTH * _CHUNK_AMO;


	//download to the buffer
	std::fstream file("saves/map.dat", std::ios::in | std::ios::binary);
	file.seekg(beg_chunk * (sizeof_Object * volume_chank));
	file.read((char*)_buff, download_volume);
	file.close();


	//from the buffer to the map
	Object *pmap = _map;
	Object *pbuff = _buff;
	for (int n = 0; n < _CHUNK_AMO; n++)
	{
		pmap = _map;
		for (int i = 0; i < _CHUNK_HEIGHT; i++)
		{
			static int row_s = _CHUNK_WIDTH * sizeof_Object;
			memcpy(pmap + n * _CHUNK_WIDTH, pbuff, row_s);
			pmap += _CHUNK_WIDTH * _CHUNK_AMO;
			pbuff += _CHUNK_WIDTH;
		}
			//system("cls");
			//for (int i = 0; i < _CHUNK_HEIGHT; i++)
			//{
			//	for (int j = 0; j < _CHUNK_AMO * _CHUNK_WIDTH; j++)
			//	{
			//		OPT temp = map[i][j].GetType();
			//		if (temp == OPT::air)std::cout << " ";
			//		else if (temp == OPT::wood)std::cout << "1";
			//		else if (temp == OPT::dirt)std::cout << "2";
			//		else if (temp == OPT::steel)std::cout << "3";
			//		else if (temp == OPT::stone)std::cout << "4";
			//	}
			//	std::cout << "\n";
			//}
		//pbuff += volume_chank;
	}


	//DEBUG
	//std::cout << "----------------------------------------";
	//for (int i = 0; i < _CHUNK_HEIGHT; i++)
	//{
	//	for (int j = 0; j < _CHUNK_AMO * _CHUNK_WIDTH; j++)
	//	{
	//		OPT temp = map[i][j].GetType();
	//		if (temp == OPT::air)std::cout << " ";
	//		else if (temp == OPT::wood)std::cout << "1";
	//		else if (temp == OPT::dirt)std::cout << "2";
	//		else if (temp == OPT::steel)std::cout << "3";
	//		else if (temp == OPT::stone)std::cout << "4";
	//	}
	//	std::cout << "\n";
	//}
	//delete[]_buff, buff; - not delete!
	is_enter = true;
}
void Map::MapUpdate(int regarding_x, int regarding_y){
	static int shifting = 70;
	int min_y = regarding_y - shifting;
	int max_y = regarding_y + shifting;
	int min_x = regarding_x - shifting;
	int max_x = regarding_x + shifting;
	for (int i = min_y; i < max_y; i++)
		for (int j = min_x; j < max_x; j++) {
			int out_i = i;
			int out_j = j;
			if (i >= _MAP_HEIGHT)out_i = _MAP_HEIGHT - 1;
			else if (i < 0)out_i = 0;
			if (j >= _MAP_WIDTH)out_j = _MAP_WIDTH - 1;
			else if (j < 0)out_j = 0;
			map[out_i][out_j].Update();
		}
}
void Map::DesertClustersGenerator(std::default_random_engine & rnd){
	for (int j = 0; j < _MAP_WIDTH; j++){
		if (GetCurrentBiome(j) == Biomes::desert) {
			int trnd = rnd() % desert_cluster_rnd;
			if (trnd == 0) {//if == 0, then we will generating the dirt-cluster int he desert
				int start_x = j;
				int width = rnd() % desert_cluster_width + j;
				if (width >= _MAP_WIDTH)width = _MAP_WIDTH - 1;

				while (width - start_x >= 0){
					int y{};
					while (y < _MAP_HEIGHT - 1 && map[y][start_x].GetType() == OPT::air)y++;//Determiniting the grass(how deep from 0(y))
				
					map[y++][start_x].SetObject(OPT::grass);//set the grass on top
					int deep = desert_cluster_deep_min + rnd() % desert_cluster_deep_max + y;
					while (y < _MAP_HEIGHT && y < deep)
						map[y++][start_x].SetObject(OPT::dirt);
					start_x++;
				}
			}
		}
	}
}
void Map::BiomeInterpolation(std::default_random_engine & rnd){
	Biomes last = Biomes::none, curr = Biomes::none;
	for (int j = 0; j < _MAP_WIDTH; j++){
		curr = GetCurrentBiome(j);
		if (curr != last) {
			int y{};
			while (y < _MAP_HEIGHT - 1 && map[y][j].GetType() == OPT::air)y++;
			OPT block = map[(y + 1 < _MAP_HEIGHT - 1 ? y + 1 : y)][j].GetType();

			while (y < _MAP_HEIGHT - 1 && map[y][j].GetType() != OPT::stone) {
				int width = biome_interpolation_min + rnd() % biome_interpolation_max;
				for (int w = 1; w <= width && j - w > 0 && map[y][j - w].GetType() != OPT::air; w++)
					map[y][j - w].SetObject(block);
				y++;
			}
		}
		last = curr;
	}
}
void Map::SaveMap(){
	//saving(in a file) the world
	if (GetFileAttributesA("saves") == 0xFFFFFFFF)
		CreateDirectory("saves", NULL);
	std::fstream file("saves/map.dat", std::ios::out | std::ios::binary);
	file.write((char*)_map, _MAP_HEIGHT * _MAP_WIDTH * sizeof(*_map));
	file.close();
}
bool Map::NewMap()
{
	//creating and saving(in a file) the world
	try
	{
		if (GetFileAttributesA("saves") == 0xFFFFFFFF)
			CreateDirectory("saves", NULL);
		std::fstream file("saves/map.dat", std::ios::out | std::ios::binary);
		std::default_random_engine rnd(CUI(time(NULL)));
		PerlinGenerator(rnd);
		BiomeInterpolation(rnd);
		DesertClustersGenerator(rnd);
		TreesPlanter(rnd);
		file.write((char*)_map, _MAP_HEIGHT * _MAP_WIDTH * sizeof(*_map));
		file.close();
		return true;
	}
	catch (...)
	{
		return false;
	}
}
bool Map::LoadMap()
{
	try
	{
		std::fstream file;
		file.open("saves/map.dat", std::ios::in | std::ios::binary);
		if (!file.is_open())throw std::exception("The map not found. Create new world, or copy-past here new map.");
		file.read((char*)_map, _MAP_HEIGHT * _MAP_WIDTH * sizeof(*_map));
		file.close();
		return true;
	}
	catch (const std::exception&ex) {
		//check for avialability of the directory
		if (GetFileAttributesA("logs") == 0xFFFFFFFF)
			CreateDirectory("logs", NULL);
		//write to file
		std::fstream file("saves/map.dat", std::ios::out | std::ios::binary);
		file.write((char*)ex.what(), strlen(ex.what()));
		file.close();
		return false;
	}
}
void Map::ClusterGenerator(int x, int y, OPT block, std::default_random_engine & rnd, int amo) {
	for (int n = 0; n < amo; n++){
		int trnd_block = rnd() % chance_of_ore;
		if(trnd_block == 0)map[y][x].SetObject(block); 
		else map[y][x].SetObject(OPT::gravel);
		int trnd = rnd() % 4;
		if (trnd == 0 && x + 1 < _MAP_WIDTH)x++;
		else if (trnd == 1 && x - 1 > 0)x--;
		else if (trnd == 2 && y + 1 < _MAP_HEIGHT-2)y++;
		else if (trnd == 3 && y - 1 > 0)y--;
	}
}
Map::~Map(){
	delete[]_map, map, biomes_coord, biomes;
}   