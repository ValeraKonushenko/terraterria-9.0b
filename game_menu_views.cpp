#include "Resourses.h"
#include "game_menu_views.h"
#include "menu_views.h"

bool game_menu_views::Update(Ms &mouse, RndWnd &wnd, int game_time, Vw &camera){
	float LSW = camera.getCenter().x - _CAMERA_WIDTH / 2 + 50.f;//left side of the window
	float USW = camera.getCenter().y - _CAMERA_HEIGHT / 2 + 50.f;//up side of the window
	float shifting = 0;
	Typemenu_views out = Typemenu_views::none;

	if (Button(mouse, wnd, Typemenu_views::Resume, LSW, USW + shifting, game_time))
		out = Typemenu_views::NewGame;
	shifting += menu_views::Sizes::NewGame::h + 12;//12 - is just some space

	if (Button(mouse, wnd, Typemenu_views::Settings, LSW, USW + shifting, game_time)
		&& out == Typemenu_views::none)
		out = Typemenu_views::ContinueGame;
	shifting += menu_views::Sizes::ContinueGame::h + 12;//12 - is just some space

	if (Button(mouse, wnd, Typemenu_views::ExitToMenu, LSW, USW + shifting, game_time)
		&& out == Typemenu_views::none)
		out = Typemenu_views::Settings;
	shifting += menu_views::Sizes::Settings::h + 12;//12 - is just some space

	if (Button(mouse, wnd, Typemenu_views::Exit, LSW, USW + shifting, game_time)
		&& out == Typemenu_views::none)
		out = Typemenu_views::Exit;


	return false;
}