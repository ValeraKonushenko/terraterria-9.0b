#include "Resourses.h"
#include "Object.h"
#include "Map.h"
#include <exception>



Object::Object():
durablity(0),
rotate(OPR::R0)
{
	SetObject(OPT::air);
}
OPT Object::GetType()const { return type; }
void Object::SetObject(OPT in_type){
	switch (in_type)
	{
	case ObjectProperties::Type::air:
		durablity = CF(OPD::air);
		this->type = OPT::air;
		break;
	case ObjectProperties::Type::steel:
		durablity = CF(OPD::steel);
		this->type = OPT::steel;
		break;
	case ObjectProperties::Type::stone:
		durablity = CF(OPD::stone);
		this->type = OPT::stone;
		break;
	case ObjectProperties::Type::dirt:
		durablity = CF(OPD::dirt);
		this->type = OPT::dirt;
		break;
	case ObjectProperties::Type::grass:
		durablity = CF(OPD::grass);
		this->type = OPT::grass;
		break;
	case ObjectProperties::Type::cobblestone:
		durablity = CF(OPD::cobblestone);
		this->type = OPT::cobblestone;
		break;
	case ObjectProperties::Type::deep_stone:
		durablity = CF(OPD::deep_stone);
		this->type = OPT::deep_stone;
		break;
	case ObjectProperties::Type::very_deep_stone:
		durablity = CF(OPD::very_deep_stone);
		this->type = OPT::very_deep_stone;
		break;
	case ObjectProperties::Type::badrock:
		durablity = CF(OPD::badrock);
		this->type = OPT::badrock;
		break;
	case ObjectProperties::Type::sandstone:
		durablity = CF(OPD::sandstone);
		this->type = OPT::sandstone;
		break;
	case ObjectProperties::Type::sand:
		durablity = CF(OPD::sand);
		this->type = OPT::sand;
		break;
	case ObjectProperties::Type::gravel:
		durablity = CF(OPD::gravel);
		this->type = OPT::gravel;
		break;
	case ObjectProperties::Type::glass:
		durablity = CF(OPD::glass);
		this->type = OPT::glass;
		break;
	case ObjectProperties::Type::bulletproof_glass:
		durablity = CF(OPD::bulletproof_glass);
		this->type = OPT::bulletproof_glass;
		break;

	/*~~~~ORES~~~~*/
	case ObjectProperties::Type::iron:
		durablity = CF(OPD::iron);
		this->type = OPT::iron;
		break;
	case ObjectProperties::Type::gold:
		durablity = CF(OPD::gold);
		this->type = OPT::gold;
		break;
	case ObjectProperties::Type::niter:
		durablity = CF(OPD::niter);
		this->type = OPT::niter;
		break;
	case ObjectProperties::Type::coal:
		durablity = CF(OPD::coal);
		this->type = OPT::coal;
		break;
	case ObjectProperties::Type::lead:
		durablity = CF(OPD::lead);
		this->type = OPT::lead;
		break;
	case ObjectProperties::Type::diamond:
		durablity = CF(OPD::diamond);
		this->type = OPT::diamond;
		break;
	case ObjectProperties::Type::tin:
		durablity = CF(OPD::tin);
		this->type = OPT::tin;
		break;
	case ObjectProperties::Type::copper:
		durablity = CF(OPD::copper);
		this->type = OPT::copper;
		break;


	/*~~~~WODD~~~~*/
	case ObjectProperties::Type::dark_oak:
		durablity = CF(OPD::dark_oak);
		this->type = OPT::dark_oak;
		break;
	case ObjectProperties::Type::dark_oak_plank:
		durablity = CF(OPD::dark_oak_plank);
		this->type = OPT::dark_oak_plank;
		break;
	case ObjectProperties::Type::dark_oak_leaves:
		durablity = CF(OPD::dark_oak_leaves);
		this->type = OPT::dark_oak_leaves;
		break;
	//spruce
	case ObjectProperties::Type::spruce:
		durablity = CF(OPD::spruce);
		this->type = OPT::spruce;
		break;
	case ObjectProperties::Type::spruce_plank:
		durablity = CF(OPD::spruce_plank);
		this->type = OPT::spruce_plank;
		break;
	case ObjectProperties::Type::spruce_leaves:
		durablity = CF(OPD::spruce_leaves);
		this->type = OPT::spruce_leaves;
		break;
	//oak
	case ObjectProperties::Type::oak:
		durablity = CF(OPD::oak);
		this->type = OPT::oak;
		break;
	case ObjectProperties::Type::oak_plank:
		durablity = CF(OPD::oak_plank);
		this->type = OPT::oak_plank;
		break;
	case ObjectProperties::Type::oak_leaves:
		durablity = CF(OPD::oak_leaves);
		this->type = OPT::oak_leaves;
		break;
	//acacia
	case ObjectProperties::Type::acacia:
		durablity = CF(OPD::acacia);
		this->type = OPT::acacia;
		break;
	case ObjectProperties::Type::acacia_plank:
		durablity = CF(OPD::acacia_plank);
		this->type = OPT::acacia_plank;
		break;
	case ObjectProperties::Type::acacia_leaves:
		durablity = CF(OPD::acacia_leaves);
		this->type = OPT::acacia_leaves;
		break;

	default:
		durablity = CF(OPD::air);
		this->type = OPT::air;
		std::string str = std::to_string(CI(in_type));
		str += " Block not found";
		throw std::exception(str.c_str());
		break;
	}
} 
Object::Object(OPT type){
	SetObject(type);
}
void Object::Update(){
	if (durablity <= 0)
		SetObject(OPT::air);
}
float Object::GetDurablity()const { return durablity; }
float Object::GetDurablityPercent()const {	
	return durablity / (GetConstDurablity(this->type) / 100);
}
void Object::SetRotate(OPR rotation) { this->rotate = rotation;  }
OPR Object::GetRotate() const{ return this->rotate; }
//float Object::GetAccumulateLoad() { return accum_load; }
//bool Object::AddAccumulateLoad(float add){
//	accum_load += add;
//	return (accum_load > GetMaxLoad(type)) ? true : false;//true - if it should fall, false - if not
//}
bool	Object::AddDurablity(float add) {
	if(durablity + add <= GetConstDurablity(this->type))
		durablity += add;
	return (durablity <= 0) ? true : false;
}
float Object::GetMaxLoad(OPT in_type)const {
	if (in_type == OPT::air)					return CF(OPL::air);
	else if (in_type == OPT::steel)				return CF(OPL::steel);
	else if (in_type == OPT::stone)				return CF(OPL::stone);
	else if (in_type == OPT::dirt)				return CF(OPL::dirt);
	else if (in_type == OPT::grass)				return CF(OPL::grass);
	else if (in_type == OPT::cobblestone)		return CF(OPL::cobblestone);
	else if (in_type == OPT::deep_stone)		return CF(OPL::deep_stone);
	else if (in_type == OPT::very_deep_stone)	return CF(OPL::very_deep_stone);
	else if (in_type == OPT::badrock)			return CF(OPL::badrock);
	else if (in_type == OPT::sandstone)			return CF(OPL::sandstone);
	else if (in_type == OPT::sand)				return CF(OPL::sand);
	else if (in_type == OPT::glass)				return CF(OPL::glass);
	else if (in_type == OPT::bulletproof_glass)	return CF(OPL::bulletproof_glass);
	else if (in_type == OPT::gravel)			return CF(OPL::gravel);
	//wood
	else if (in_type == OPT::dark_oak)			return CF(OPL::dark_oak);
	else if (in_type == OPT::dark_oak_plank)	return CF(OPL::dark_oak_plank);
	else if (in_type == OPT::dark_oak_leaves)	return CF(OPL::dark_oak_leaves);
	else if (in_type == OPT::spruce)			return CF(OPL::spruce);
	else if (in_type == OPT::spruce_plank)		return CF(OPL::spruce_plank);
	else if (in_type == OPT::spruce_leaves)		return CF(OPL::spruce_leaves);
	else if (in_type == OPT::oak)				return CF(OPL::oak);
	else if (in_type == OPT::oak_plank)			return CF(OPL::oak_plank);
	else if (in_type == OPT::oak_leaves)		return CF(OPL::oak_leaves);
	else if (in_type == OPT::acacia)			return CF(OPL::acacia);
	else if (in_type == OPT::acacia_plank)		return CF(OPL::acacia_plank);
	else if (in_type == OPT::acacia_leaves)		return CF(OPL::acacia_leaves);
	//ores
	else if (in_type == OPT::iron)				return CF(OPL::iron);
	else if (in_type == OPT::gold)				return CF(OPL::gold);
	else if (in_type == OPT::niter)				return CF(OPL::niter);
	else if (in_type == OPT::coal)				return CF(OPL::coal);
	else if (in_type == OPT::lead)				return CF(OPL::lead);
	else if (in_type == OPT::diamond)			return CF(OPL::diamond);
	else if (in_type == OPT::tin)				return CF(OPL::tin);
	else if (in_type == OPT::copper)			return CF(OPL::copper);
	else {
		std::string str = std::to_string(CI(in_type));
		str += " Block not found";
		throw std::exception(str.c_str());
	}
}
float Object::GetConstDurablity(OPT in_type)const {
	if		(in_type == OPT::air)				return CF(OPD::air);
	else if (in_type == OPT::steel)				return CF(OPD::steel);
	else if (in_type == OPT::stone)				return CF(OPD::stone);
	else if (in_type == OPT::dirt)				return CF(OPD::dirt);
	else if (in_type == OPT::grass)				return CF(OPD::grass);
	else if (in_type == OPT::cobblestone)		return CF(OPD::cobblestone);
	else if (in_type == OPT::deep_stone)		return CF(OPD::deep_stone);
	else if (in_type == OPT::very_deep_stone)	return CF(OPD::very_deep_stone);
	else if (in_type == OPT::badrock)			return CF(OPD::badrock);
	else if (in_type == OPT::sandstone)			return CF(OPD::sandstone);
	else if (in_type == OPT::sand)				return CF(OPD::sand);
	else if (in_type == OPT::glass)				return CF(OPD::glass);
	else if (in_type == OPT::bulletproof_glass)	return CF(OPD::bulletproof_glass);
	else if (in_type == OPT::gravel)			return CF(OPD::gravel);
	//wood
	else if (in_type == OPT::dark_oak)			return CF(OPD::dark_oak);
	else if (in_type == OPT::dark_oak_plank)	return CF(OPD::dark_oak_plank);
	else if (in_type == OPT::dark_oak_leaves)	return CF(OPD::dark_oak_leaves);
	else if (in_type == OPT::spruce)			return CF(OPD::spruce);
	else if (in_type == OPT::spruce_plank)		return CF(OPD::spruce_plank);
	else if (in_type == OPT::spruce_leaves)		return CF(OPD::spruce_leaves);
	else if (in_type == OPT::oak)				return CF(OPD::oak);
	else if (in_type == OPT::oak_plank)			return CF(OPD::oak_plank);
	else if (in_type == OPT::oak_leaves)		return CF(OPD::oak_leaves);
	else if (in_type == OPT::acacia)			return CF(OPD::acacia);
	else if (in_type == OPT::acacia_plank)		return CF(OPD::acacia_plank);
	else if (in_type == OPT::acacia_leaves)		return CF(OPD::acacia_leaves);
	//ores
	else if (in_type == OPT::iron)				return CF(OPD::iron);
	else if (in_type == OPT::gold)				return CF(OPD::gold);
	else if (in_type == OPT::niter)				return CF(OPD::niter);
	else if (in_type == OPT::coal)				return CF(OPD::coal);
	else if (in_type == OPT::lead)				return CF(OPD::lead);
	else if (in_type == OPT::diamond)			return CF(OPD::diamond);
	else if (in_type == OPT::tin)				return CF(OPD::tin);
	else if (in_type == OPT::copper)			return CF(OPD::copper);
	else {
		std::string str = std::to_string(CI(in_type));
		str += " Block not found";
		throw std::exception(str.c_str());
	}
}
float Object::GetConstMass(OPT in_type)const {
	if (in_type == OPT::air)					return CF(OPM::air);
	else if (in_type == OPT::steel)				return CF(OPM::steel);
	else if (in_type == OPT::stone)				return CF(OPM::stone);
	else if (in_type == OPT::dirt)				return CF(OPM::dirt);
	else if (in_type == OPT::grass)				return CF(OPM::grass);
	else if (in_type == OPT::cobblestone)		return CF(OPM::cobblestone);
	else if (in_type == OPT::deep_stone)		return CF(OPM::deep_stone);
	else if (in_type == OPT::very_deep_stone)	return CF(OPM::very_deep_stone);
	else if (in_type == OPT::badrock)			return CF(OPM::badrock);
	else if (in_type == OPT::sandstone)			return CF(OPM::sandstone);
	else if (in_type == OPT::sand)				return CF(OPM::sand);
	else if (in_type == OPT::glass)				return CF(OPM::glass);
	else if (in_type == OPT::bulletproof_glass)	return CF(OPM::bulletproof_glass);
	else if (in_type == OPT::gravel)			return CF(OPM::gravel);
	//wood
	else if (in_type == OPT::dark_oak)			return CF(OPM::dark_oak);
	else if (in_type == OPT::dark_oak_plank)	return CF(OPM::dark_oak_plank);
	else if (in_type == OPT::dark_oak_leaves)	return CF(OPM::dark_oak_leaves);
	else if (in_type == OPT::spruce)			return CF(OPM::spruce);
	else if (in_type == OPT::spruce_plank)		return CF(OPM::spruce_plank);
	else if (in_type == OPT::spruce_leaves)		return CF(OPM::spruce_leaves);
	else if (in_type == OPT::oak)				return CF(OPM::oak);
	else if (in_type == OPT::oak_plank)			return CF(OPM::oak_plank);
	else if (in_type == OPT::oak_leaves)		return CF(OPM::oak_leaves);
	else if (in_type == OPT::acacia)			return CF(OPM::acacia);
	else if (in_type == OPT::acacia_plank)		return CF(OPM::acacia_plank);
	else if (in_type == OPT::acacia_leaves)		return CF(OPM::acacia_leaves);
	//ores
	else if (in_type == OPT::iron)				return CF(OPM::iron);
	else if (in_type == OPT::gold)				return CF(OPM::gold);
	else if (in_type == OPT::niter)				return CF(OPM::niter);
	else if (in_type == OPT::coal)				return CF(OPM::coal);
	else if (in_type == OPT::lead)				return CF(OPM::lead);
	else if (in_type == OPT::diamond)			return CF(OPM::diamond);
	else if (in_type == OPT::tin)				return CF(OPM::tin);
	else if (in_type == OPT::copper)			return CF(OPM::copper);
	else {
		std::string str = std::to_string(CI(in_type));
		str += " Block not found";
		throw std::exception(str.c_str());
	}
}